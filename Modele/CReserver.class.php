<?php

/**
 * Classe representant une reservation
 * @author Manon Cottart
 */
class CReserver {

    /**
     * Attribut representant la reference d'un materiel
     */
    private $ref;

    /**
     * Attribut representant le matricule d'un utilisateur
     */
    private $matricule;

    /**
     * Attribut representant le debut de la reservation
     */
    private $debut_reservation;

    /**
     * Attribut representant la fin de la reservation
     */
    private $fin_reservation;

    /**
     * Constructeur de la classe 
     */
    public function __construct($refParam,$matriculeParam,$debut_reservationParam,$fin_reservationParam){
        $this->ref = $refParam;
        $this->matricule = $matriculeParam;
        $this->debut_reservation = $debut_reservationParam;
        $this->fin_reservation = $fin_reservationParam;
    }
    
    /**
     * Getter de l'attribut ref
     * @return ref la reference du materiel
     */ 
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * Setter de l'attribut ref
     * @param refParam une reference
     */ 
    public function setRef($refParam)
    {
        $this->ref = $refParam;
    }

    /**
     * Getter de l'attribut matricule
     * @return matricule le matricule de l'utilisateur
     */ 
    public function getMatricule()
    {
        return $this->matricule;
    }

    /**
     * Setter de l'attribut matricule
     * @param matriculeParam un matricule
     */ 
    public function setMatricule($matriculeParam)
    {
        $this->matricule = $matriculeParam;
    }

    /**
     * Getter de l'attribut debut_reservation
     * @return debut_reservation le debut de la reservation
     */ 
    public function getDebut_reservation()
    {
        return $this->debut_reservation;
    }

    /**
     * Setter de l'attribut debut_reservation
     * @param debut_reservationParam un debut de reservation
     */ 
    public function setDebut_reservation($debut_reservationParam)
    {
        $this->debut_reservation = $debut_reservationParam;
    }

    /**
     * Getter de l'attribut fin_reservation
     * @return fin_reservation la fin de la reservation
     */ 
    public function getFin_reservation()
    {
        return $this->fin_reservation;
    }

    /**
     * Setter de l'attribut fin_reservation
     * @param fin_reservationParam une fin de reservation
     */ 
    public function setFin_reservation($fin_reservationParam)
    {
        $this->fin_reservation = $fin_reservationParam;
    }
}

?>