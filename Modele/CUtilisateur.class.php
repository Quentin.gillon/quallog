<?php

/**
 * Classe representant un utilisateur
 * @author Manon Cottart
 */
class CUtilisateur {

    /**
     * Attribut representant le matricule de l'utilisateur
     */
    private $matricule;

    /**
     * Attribut representant le nom de l'utilisateur
     */
    private $nom;

    /**
     * Attribut representant le prenom de l'utilisateur
     */
    private $prenom;

    /**
     * Attribut representant le role de l'utilisateur
     */
    private $role;

    /**
     * Attribut representant l'email de l'utilisateur
     */
    private $email;

    /**
     * Constructeur de la classe 
     */
    public function __construct($matriculeParam,$nomParam,$prenomParam,$roleParam,$emailParam){
        $this->matricule = $matriculeParam;
        $this->nom = $nomParam;
        $this->prenom = $prenomParam;
        $this->role = $roleParam;
        $this->email = $emailParam;
    }

    /**
     * Getter de l'attribut matricule
     * @return matricule le matricule de l'utilisateur
     */
    public function getMatricule(){
        return $this->matricule;
    }

    /**
     * Getter de l'attribut nom
     * @return nom le nom de l'utilisateur
     */
    public function getNom(){
        return $this->nom;
    }

    /**
     * Getter de l'attribut prenom
     * @return prenom le prenom de l'utilisateur
     */
    public function getPrenom(){
        return $this->prenom;
    }

    /**
     * Getter de l'attribut role
     * @return role le role de l'utilisateur
     */
    public function getRole(){
        return $this->role;
    }

    /**
     * Getter de l'attribut email
     * @return email l'email de l'utilisateur
     */
    public function getEmail(){
        return $this->email;
    }

    /**
     * Setter de l'attribut matricule
     * @param matriculeParam un matricule
     */
    public function setMatricule($matriculeParam){
        $this->matricule = $matriculeParam;
    }

    /**
     * Setter de l'attribut nom
     * @param nomParam un nom
     */
    public function setNom($nomParam){
        $this->nom = $nomParam;
    }

    /**
     * Setter de l'attribut prenom
     * @param prenomParam un prenom
     */
    public function setPrenom($prenomParam){
        $this->prenom = $prenomParam;
    }

    /**
     * Setter de l'attribut role
     * @param roleParam un role
     */
    public function setRole($roleParam){
        $this->role = $roleParam;
    }

    /**
     * Setter de l'attribut email
     * @param emailParam un email
     */
    public function setEmail($emailParam){
        $this->email = $emailParam;
    }
}

?>