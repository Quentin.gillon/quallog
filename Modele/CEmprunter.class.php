<?php

/**
 * Classe representant un emprunt
 * @author Manon Cottart
 */
class CEmprunter {

    /**
     * Attribut representant la reference d'un materiel
     */
    private $ref;

    /**
     * Attribut representant le matricule d'un utilisateur
     */
    private $matricule;

    /**
     * Attribut representant le debut de l'emprunt
     */
    private $debut_emprunt;

    /**
     * Attribut representant la fin de l'emprunt
     */
    private $fin_emprunt;

    /**
     * Constructeur de la classe 
     */
    public function __construct($refParam,$matriculeParam,$debut_empruntParam,$fin_empruntParam){
        $this->ref = $refParam;
        $this->matricule = $matriculeParam;
        $this->debut_emprunt = $debut_empruntParam;
        $this->fin_emprunt = $fin_empruntParam;
    }

    /**
     * Getter de l'attribut ref
     * @return ref la reference du materiel
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * Setter de l'attribut ref
     * @param refParam une reference
     */ 
    public function setRef($refParam)
    {
        $this->ref = $refParam;
    }

    /**
     * Getter de l'attribut matricule
     * @return matricule le matricule de l'utilisateur
     */ 
    public function getMatricule()
    {
        return $this->matricule;
    }

    /**
     * Setter de l'attribut matricule
     * @param matriculeParam un matricule
     */ 
    public function setMatricule($matriculeParam)
    {
        $this->matricule = $matriculeParam;
    }

    /**
     * Getter de l'attribut debut_emprunt
     * @return debut_emprunt le debut de l'emprunt
     */ 
    public function getDebut_emprunt()
    {
        return $this->debut_emprunt;
    }

    /**
     * Setter de l'attribut debut_emprunt
     * @param debut_empruntParam un debut d'emprunt
     */ 
    public function setDebut_emprunt($debut_empruntParam)
    {
        $this->debut_emprunt = $debut_empruntParam;
    }

    /**
     * Getter de l'attribut fin_emprunt
     * @return fin_emprunt la fin de l'emprunt
     */ 
    public function getFin_emprunt()
    {
        return $this->fin_emprunt;
    }

    /**
     * Setter de l'attribut fin_emprunt
     * @param fin_empruntParam une fin d'emprunt
     */ 
    public function setFin_emprunt($fin_empruntParam)
    {
        $this->fin_emprunt = $fin_empruntParam;
    }
}

?>