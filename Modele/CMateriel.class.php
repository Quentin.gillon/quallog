<?php

/**
 * Classe representant un materiel
 * @author Manon Cottart
 */
class CMateriel {

    /**
     * Attribut representant le nom du materiel
     */
    private $nom;

    /**
     * Attribut representant la version du materiel
     */
    private $version;

    /**
     * Attribut representant la reference du materiel
     */
    private $ref;

    /**
     * Attribut representant le type de materiel
     */
    private $type;

    /**
     * Attribut representant le statut du materiel
     */
    private $statut;

    /**
     * Attribut representant le lien vers l'image
     */
    private $photo;

     /**
     * Attribut representant le numero de telephone du materiel (si telephone portable)
     */
    private $numtel;


    /**
     * Constructeur de la classe 
     */
    public function __construct($nomParam,$versionParam,$refParam,$typeParam,$statutParam,$photoParam,$numtelParam){
        $this->nom = $nomParam;
        $this->version = $versionParam;
        $this->ref = $refParam;
        $this->type = $typeParam;
        $this->statut = $statutParam;
        $this->photo = $photoParam;
        $this->numtel = $numtelParam;
    }

    /**
     * Getter de l'attribut nom
     * @return nom le nom du materiel
     */
    public function getNom(){
        return $this->nom;
    }

    /**
     * Getter de l'attribut version
     * @return version le nom du materiel
     */
    public function getVersion(){
        return $this->version;
    }

    /**
     * Getter de l'attribut ref
     * @return ref la reference du materiel
     */
    public function getRef(){
        return $this->ref;
    }

    /**
     * Getter de l'attribut type
     * @return type le type de materiel
     */
    public function getType(){
        return $this->type;
    }

    /**
     * Getter de l'attribut statut
     * @return statut le statut du materiel
     */
    public function getStatut(){
        return $this->statut;
    }

    /**
     * Getter de l'attribut photo
     * @return photo le lien de la photo du materiel 
     */
    public function getPhoto(){
        return $this->photo;
    }

    /**
     * Getter de l'attribut numtel
     * @return numtel le numero de telephone du materiel 
     */
    public function getNumtel(){
        return $this->numtel;
    }

    /**
     * Setter de l'attribut nom
     * @param nomParam un nom de materiel
     */
    public function setNom($nomParam){
        $this->nom = $nomParam;
    }

    /**
     * Setter de l'attribut version
     * @param versionParam une version de materiel
     */
    public function setVersion($versionParam){
        $this->version = $versionParam;
    }

    /**
     * Setter de l'attribut ref
     * @param refParam une reference de materiel
     */
    public function setRef($refParam){
        $this->ref = $refParam;
    }

    /**
     * Setter de l'attribut type
     * @param typeParam un type de materiel
     */
    public function setType($typeParam){
        $this->type = $typeParam;
    }

    /**
     * Setter de l'attribut statut
     * @param statutParam une statutnibilite de materiel
     */
    public function setStatut($statutParam){
        $this->statut = $statutParam;
    }

    /**
     * Setter de l'attribut photo
     * @param photoParam un lien de photo de materiel 
     */
    public function setPhoto($photoParam){
        $this->photo = $photoParam;
    }

    /**
     * Setter de l'attribut numtel
     * @param numtelParam un numero de telephone de materiel 
     */
    public function setNumtel($numtelParam){
        $this->numtel = $numtelParam;
    }
}

?>