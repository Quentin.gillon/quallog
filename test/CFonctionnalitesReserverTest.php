<?php

//import
use PHPUnit\Framework\TestCase;
require_once("../Controleur/CFonctionnalitesReserver.class.php");

/**
 * Classe de test de la classe CFonctionnalitesReserver
 * @author Mael Pons
 */
class CFonctionnalitesReserverTest extends TestCase
{

/*
    public function testGetBdd()
    {

    }

    public function testSetReservation()
    {

    }*/

    /**
     * test de la methode GetReservation
     */
    public function testGetReservation()
    {
        
        $reservationTest= $this->createMock(CReserver::class);
        $fctReservationTest= new CFonctionnalitesReserver($reservationTest);
        $this->assertEquals($reservationTest,$fctReservationTest->getReservation());

    }
/*
    public function testSetBdd()
    {

    }

    public function testReserver()
    {

    }*/
}
