<?php

//import
use PHPUnit\Framework\TestCase;
require_once('../Modele/CUtilisateur.class.php');

/**
 * Classe de test de la classe CUtilisateur
 * @author Honorine Chantre
 */
Class CUtilisateurTest extends TestCase{

    /**
     * Test de la methode getMatricule
     */
    public function testGetMatricule()
    {
        $user = new CUtilisateur(12345,"dupond","martin","admin","dupond.martin@test.fr");
        $value = 12345;
        $this->assertEquals($value, $user->getMatricule());
    }

    /**
     * Test de la methode setMatricule
     * @depends testGetMatricule
     */
    public function testSetMatricule()
    {
        $user = new CUtilisateur(12345,"dupond","martin","admin","dupond.martin@test.fr");
        $value = 12346789;
        $user->setMatricule($value);
        $this->assertEquals($value, $user->getMatricule());
    }


    /**
     * Test de la methode getNom
     */
    public function testGetNom()
    {
        $user = new CUtilisateur(12345,"dupond","martin","admin","dupond.martin@test.fr");
        $value = "dupond";
        $this->assertEquals($value, $user->getNom());
    }

    /**
     * Test de la methode setNom
     * @depends testGetNom
     */
    public function testValidSetNom()
    {
        $user = new CUtilisateur(12345, "dupond", "martin", "admin", "dupond.martin@test.fr");
        $value = "dupont";
        $user->setNom($value);
        $this->assertEquals($value, $user->getNom());
    }


    /**
     * Test de la methode getPrenom
     */
    public function testGetPrenom()
    {
        $user = new CUtilisateur(12345,"dupond","martin","admin","dupond.martin@test.fr");
        $value = "martin";
        $this->assertEquals($value, $user->getPrenom());
    }

    /**
     * Test de la methode setPrenom
     * @depends testGetPrenom
     */
    public function testSetPrenom()
    {
        $user = new CUtilisateur(12345,"dupond","martin","admin","dupond.martin@test.fr");
        $value = "pierre";
        $user->setPrenom($value);
        $this->assertEquals($value, $user->getPrenom());
    }


    /**
     * Test de la methode getRole
     */
    public function testGetRole()
    {
        $user = new CUtilisateur(12345,"dupond","martin","admin","dupond.martin@test.fr");
        $value = "admin";
        $this->assertEquals($value, $user->getRole());
    }

    /**
     * Test de la methode setRole
     * @depends testGetRole
     */
    public function testSetRole()
    {
        $user = new CUtilisateur(12345,"dupond","martin","admin","dupond.martin@test.fr");
        $value = "emprunteur";
        $user->setRole($value);
        $this->assertEquals($value, $user->getRole());
    }


    /**
     * Test de la methode getEmail
     */
    public function testGetEmail()
    {
        $user = new CUtilisateur(12345,"dupond","martin","admin","dupond.martin@test.fr");
        $value = "dupond.martin@test.fr";
        $this->assertEquals($value, $user->getEmail());
    }

    /**
     * Test de la methode setEmail
     * @depends testGetEmail
     */
    public function testSetEmail()
    {
        $user = new CUtilisateur(12345,"dupond","martin","admin","dupond.martin@test.fr");
        $value = "test@test.fr";
        $user->setEmail($value);
        $this->assertEquals($value, $user->getEmail());
    }
}
?>