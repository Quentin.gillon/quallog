<?php

//import
use PHPUnit\Framework\TestCase;
require_once('../Modele/CReserver.class.php');

/**
 * Classe de test de la classe CReserver
 * @author Mael Pons
 */
Class CReserverTest extends TestCase{


    /**
     * test de la methode GetRef
     */
    public function testGetRef(){

        $reservationTest= new CReserver(1,2,"12/12/2020","20/12/2020");
        $this->assertTrue(1==$reservationTest->getRef());

    }

    /**
     * test de la methode GetMatricule
     */
    public function testGetMatricule(){

        $reservationTest= new CReserver(1,2,"12/12/2020","20/12/2020");
        $this->assertTrue(2==$reservationTest->getMatricule());

    }

    /**
     * test de la methode GetDebut_reservation
     */
    public function testGetDebut_reservation(){

        $reservationTest= new CReserver(1,2,"12/12/2020","20/12/2020");
        $this->assertTrue("12/12/2020"==$reservationTest->getDebut_reservation());

    }

    /**
     * test de la methode GetFin_reservation
     */
    public function testGetFin_reservation(){

        $reservationTest= new CReserver(1,2,"12/12/2020","20/12/2020");
        $this->assertTrue("20/12/2020"==$reservationTest->getFin_reservation());

    }

    /**
     * test de la methode SetRef
     * @depends testGetRef
     */
    public function testSetRef(){

        $reservationTest= new CReserver(1,2,"12/12/2020","20/12/2020");
        $value=6;
        $reservationTest->setRef($value);
        $this->assertTrue($value==$reservationTest->getRef());

    }

    /**
     * test de la methode SetMatricule
     * @depends testGetMatricule
     */
    public function testSetMatricule(){

        $reservationTest= new CReserver(1,2,"12/12/2020","20/12/2020");
        $value=154;
        $reservationTest->setMatricule($value);
        $this->assertTrue($value==$reservationTest->getMatricule());

    }

    /**
     * test de la methode SetDebut_reservation
     * @depends testGetDebut_reservation
     */
    public function testSetDebut_reservation(){

        $reservationTest= new CReserver(1,2,"12/12/2020","20/12/2020");
        $value="15/12/2020";
        $reservationTest->setDebut_reservation($value);
        $this->assertTrue($value==$reservationTest->getDebut_reservation());

    }

    /**
     * test de la methode SetFin_reservation
     * @depends testGetFin_reservation
     */
    public function testSetFin_reservation(){

        $reservationTest= new CReserver(1,2,"12/12/2020","20/12/2020");
        $value="17/12/2020";
        $reservationTest->setFin_reservation($value);
        $this->assertTrue($value==$reservationTest->getFin_reservation());

    }


}


?>