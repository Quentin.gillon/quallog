<?php

//import
use PHPUnit\Framework\TestCase;
require_once('../Modele/CEmprunter.class.php');

/**
 * Classe de test de la classe CEmprunter
 * @author Mael Pons
 */
Class CEmprunterTest extends TestCase{

    /**
     * Test de la fonction getRef
     */
    public function testGetRef(){
        $empruntTest= new CEmprunter(1,2,"01/12/2020","12/12/2020");
        $this->assertTrue(1==$empruntTest->getRef());
    }

    /**
     * Test de la fonction getMatricule
     */
    public function testGetMatricule(){
        $empruntTest= new CEmprunter(1,2,"01/12/2020","12/12/2020");
        $this->assertTrue(2==$empruntTest->getMatricule());
    }

    /**
     * Test de la fonction getDebut_Emprunt
     */
    public function testGetDebut_Emprunt(){
        $empruntTest= new CEmprunter(1,2,"01/12/2020","12/12/2020");
        $this->assertTrue("01/12/2020"==$empruntTest->getDebut_emprunt());
    }

    /**
     * Test de la fonction getFin_emprunt
     */
    public function testGetFin_emprunt(){
        $empruntTest= new CEmprunter(1,2,"01/12/2020","12/12/2020");
        $this->assertTrue("12/12/2020"==$empruntTest->getFin_emprunt());
    }

    /**
     * Test de la fonction setRef
     * @depends testGetRef
     */
    public function testSetRef(){
        $empruntTest= new CEmprunter(1,2,"01/12/2020","12/12/2020");
        $value = 89;
        $empruntTest->setRef($value);
        $this->assertTrue(89==$empruntTest->getRef());
    }

    /**
     * Test de la fonction setMatricule
     * @depends testGetMatricule
     */
    public function testSetMatricule(){
        $empruntTest= new CEmprunter(1,2,"01/12/2020","12/12/2020");
        $value = 89;
        $empruntTest->setMatricule($value);
        $this->assertEquals($value, $empruntTest->getMatricule());
    }

    /**
     * Test de la fonction setDebut_emprunt
     */
    public function testSetDebut_emprunt(){
        $empruntTest= new CEmprunter(1,2,"01/12/2020","12/12/2020");
        $value="15/12/2020";
        $empruntTest->setDebut_emprunt($value);
        $this->assertEquals($value, $empruntTest->getDebut_emprunt());
    }

    /**
     * Test de la fonction setFin_emprunt
     * @depends testGetFin_emprunt
     */
    public function testSetFin_emprunt(){
        $empruntTest= new CEmprunter(1,2,"01/12/2020","12/12/2020");
        $value="25/12/2020";
        $empruntTest->setFin_emprunt($value);
        $this->assertEquals($value,$empruntTest->getFin_emprunt());
    }

}


?>