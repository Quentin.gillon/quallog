<?php

//import
use PHPUnit\Framework\TestCase;
require_once("../Controleur/CFonctionnalitesUtilisateur.class.php");

/**
 * Classe de test de la classe CFonctionnalitesUtilisateur
 * @author Mael Pons
 */
class CFonctionnalitesUtilisateurTest extends TestCase
{
    /**
     * test de la methode GetUtilisateur
     */
    public function testGetUtilisateur()
    {
        $utilisateurTest= $this->createMock(CUtilisateur::class);
        $fctUtilisateurTest= new CFonctionnalitesUtilisateur($utilisateurTest);
        $this->assertEquals($utilisateurTest,$fctUtilisateurTest->getUtilisateur());

    }

    /*public function testModifierUtilisateur()
    {

    }

    public function testConsulterDetailUtilisateur()
    {

    }

    public function testConsulterUtilisateurs()
    {

    }

    public function testCreerUtilisateur()
    {

    }

    public function testGetBdd()
    {

    }*/
}
