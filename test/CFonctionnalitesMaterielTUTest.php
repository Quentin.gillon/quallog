<?php

//import
use PHPUnit\Framework\TestCase;
require_once('../Controleur/CFonctionnalitesMateriel.class.php');
require_once("../Controleur/CFonctionnalitesMateriel.class.php");

/**
 * Classe de test unitaire de la classe CFonctionnalitesMateriel
 * @author Mael Pons et Honorine Chantre
 */
class CFonctionnalitesMaterielTUTest extends TestCase
{
    /**
     * Test la la consultation dun matériel dans la bdd
     * Manque des mocks
     */
    public function testConsulterMateriels()
    {
        $pDBB= new CBdd();

        try
        {
            $pDBB->creer_bdd();
        } catch(Exception $e){
            die('connexion echouee : '.$e->getMessage())."<br/>";
        }
        $materielTest= new CMateriel("Samsung","SAMG10",1234,"portable",0,"www.google.com/image/","0767473970");
        $fctMateriel= new CFonctionnalitesMateriel($materielTest);
        $fctMateriel->creerMateriel();
        $consultation=$fctMateriel->consulterMateriels();
        $this->assertEquals($materielTest->getNom(),$consultation[0]['nom']);
        $this->assertTrue($materielTest->getType()==$consultation[0]['type']);
        $this->assertTrue($materielTest->getRef()==$consultation[0]['reference']);
        $this->assertTrue($materielTest->getVersion()==$consultation[0]['version']);
        $this->assertTrue($materielTest->getPhoto()==$consultation[0]['photo']);
        $this->assertTrue($materielTest->getType()==$consultation[0]['type']);
        $this->assertTrue($materielTest->getStatut()==$consultation[0]['statut']);
        $fctMateriel->supprimerMateriel(1234);

    }

    /**
     * Test la création d'un materiel dans la bdd
     * Manque des mocks
     * @depends testConsulterMateriels
     */
    public function testCreerMateriel()
    {
        $pDBB= new CBdd();

        try
        {
            $pDBB->creer_bdd();
        } catch(Exception $e){
            die('connexion echouee : '.$e->getMessage())."<br/>";
        }
        $materielTest= new CMateriel("Samsung","SAMG10",1234,"portable",0,"www.google.com/image/","0767473970");
        $fctMateriel= new CFonctionnalitesMateriel($materielTest);
        $fctMateriel->creerMateriel();
        $consultation=$fctMateriel->consulterMateriels();
        $this->assertTrue($materielTest->getNom()==$consultation[0]['nom']);
        $this->assertTrue($materielTest->getType()==$consultation[0]['type']);
        $this->assertTrue($materielTest->getRef()==$consultation[0]['reference']);
        $this->assertTrue($materielTest->getVersion()==$consultation[0]['version']);
        $this->assertTrue($materielTest->getPhoto()==$consultation[0]['photo']);
        $this->assertTrue($materielTest->getType()==$consultation[0]['type']);
        $this->assertTrue($materielTest->getStatut()==$consultation[0]['statut']);
        $fctMateriel->supprimerMateriel(1234);

    }

    /**
     * Test la mofification d'un materiel dans la bdd
     * Manque des mocks
     */
    public function testModifierMateriel()
    {
        $pDBB= new CBdd();

        try
        {
            $pDBB->creer_bdd();
        } catch(Exception $e){
            die('connexion echouee : '.$e->getMessage())."<br/>";
        }
        $materielTest= new CMateriel("Samsung","SAMG10",1234,"portable",0,"www.google.com/image/","0767473970");
        $materielTest2= new CMateriel("iphone","123",1234,"portable",0,"www.google.com/image/","0767488970");
        $fctMateriel= new CFonctionnalitesMateriel($materielTest);
        $fctMateriel->creerMateriel();
        $fctMateriel->setMateriel($materielTest2);
        $fctMateriel->modifierMateriel();
        $consultation=$fctMateriel->consulterMateriels();
        $this->assertEquals($materielTest2->getNom(),$consultation[0]['nom']);
        $this->assertTrue($materielTest2->getType()==$consultation[0]['type']);
        $this->assertTrue($materielTest2->getRef()==$consultation[0]['reference']);
        $this->assertTrue($materielTest2->getVersion()==$consultation[0]['version']);
        $this->assertTrue($materielTest2->getPhoto()==$consultation[0]['photo']);
        $this->assertTrue($materielTest2->getType()==$consultation[0]['type']);
        $this->assertTrue($materielTest2->getStatut()==$consultation[0]['statut']);
        $fctMateriel->supprimerMateriel(1234);
        $fctMateriel->supprimerMateriel(78934);

    }

    /*public function testGetBdd()
    {

    }



    public function testSetBdd()
    {

    }*/

    /**
     * Test modification de statue d'un materiel dans la bdd
     * Manque des mocks
     */
    public function testRendreDisponibleMateriel()
    {
        $pDBB= new CBdd();

        try
        {
            $pDBB->creer_bdd();
        } catch(Exception $e){
            die('connexion echouee : '.$e->getMessage())."<br/>";
        }
        $materielTest= new CMateriel("Samsung","SAMG10",1234,"portable",0,"www.google.com/image/","0767473970");
        $fctMateriel= new CFonctionnalitesMateriel($materielTest);
        $fctMateriel->creerMateriel();
        $fctMateriel->rendreDisponibleMateriel($materielTest->getRef());
        $consultation=$fctMateriel->consulterMateriels();
        $this->assertTrue($consultation[0]['statut']==1);

    }

    /**
     * Test la modification d'un materieldans l'objet CfctMateriel
     */
    public function testSetMateriel()
    {
        $pDBB= new CBdd();

        try
        {
            $pDBB->creer_bdd();
        } catch(Exception $e){
            die('connexion echouee : '.$e->getMessage())."<br/>";
        }
        $materielTest = $this->createMock(CMateriel::class);
        $fctMateriel= new CFonctionnalitesMateriel($materielTest);
        $fctMateriel->creerMateriel();
        $this->assertEquals($materielTest, $fctMateriel->getMateriel());
        $fctMateriel->supprimerMateriel(1234);

    }

    /*public function testConsulterDetailMateriel()
    {


    }*/

    /**
     * Test la suppression d'un materiel dans la bdd
     * Manque des mocks
     */
    public function testSupprimerMateriel()
    {
        $pDBB= new CBdd();

        try
        {
            $pDBB->creer_bdd();
        } catch(Exception $e){
            die('connexion echouee : '.$e->getMessage())."<br/>";
        }
        $materielTest= new CMateriel("Samsung","SAMG10",1234,"portable",0,"www.google.com/image/","0767473970");
        $fctMateriel= new CFonctionnalitesMateriel($materielTest);
        $fctMateriel->creerMateriel();
        $fctMateriel->supprimerMateriel(1234);
        $sql = "SELECT COUNT(*) FROM Materiel";
        $result=$fctMateriel->getBdd()->lire($sql);
        $this->assertEquals(NULL,$result==NULL);

    }
}
