<?php

//import
use PHPUnit\Framework\TestCase;
require_once('../Modele/CMateriel.class.php');

/**
 * Classe de test de la classe CMateriel
 * @author Mael Pons & Honorine Chantre
 */
Class CMaterielTest extends TestCase{

    /**
     * Test de la methode getNom
     */
    public function testGetNom(){
        $CMaterielTest= new CMateriel("Samsung","SAMG10",1234,"portable",0,"www.google.com/image/","0767473970");
        $value = "Samsung";
        $this->assertEquals($value,$CMaterielTest->getNom());
    }

    /**
     * Test de la methode getVersion
     */
    public function testGetVersion(){
        $CMaterielTest= new CMateriel("Samsung","SAMG10",1234,"portable",0,"www.google.com/image/","0767473970");
        $value = "SAMG10";
        $this->assertEquals($value,$CMaterielTest->getVersion());

    }

    /**
     * Test de la methode getRef
     */
    public function testGetRef(){
        $CMaterielTest= new CMateriel("Samsung","SAMG10",1234,"portable",0,"www.google.com/image/","0767473970");
        $value=1234;
        $this->assertEquals($value,$CMaterielTest->getRef());
        
    }

    /**
     * Test de la methode getType
     */
    public function testGetType(){
        $CMaterielTest= new CMateriel("Samsung","SAMG10",1234,"portable",0,"www.google.com/image/","0767473970");
        $value="portable";
        $this->assertEquals($value,$CMaterielTest->getType());
        
    }

    /**
     * Test de la methode getTel
     */
    public function testGetTel(){
        $CMaterielTest= new CMateriel("Samsung","SAMG10",1234,"portable",0,"www.google.com/image/","0767473970");
        $value="0767473970";
        $this->assertEquals($value,$CMaterielTest->getNumtel());
        
    }

    /**
     * Test de la methode getPhoto
     */
    public function testGetPhoto(){
        $CMaterielTest= new CMateriel("Samsung","SAMG10",1234,"portable",0,"www.google.com/image/","0767473970");
        $value="www.google.com/image/";
        $this->assertEquals($value,$CMaterielTest->getPhoto());
        
    }

    /**
     * Test de la methode setNom
     * @depends testGetNom
     */
    public function testSetNom(){
        $CMaterielTest= new CMateriel("Samsung","SAMG10",1234,"portable",0,"www.google.com/image/","0767473970");
        $CMaterielTest->setNom("lucien");
        $this->assertSame("lucien",$CMaterielTest->getNom());

    }

    /**
     * Test de la methode setVersion
     * @depends testGetVersion
     */
    public function testSetVersion(){
        $CMaterielTest= new CMateriel("Samsung","SAMG10",1234,"portable",0,"www.google.com/image/","0767473970");
        $value="SAMG11";
        $CMaterielTest->setVersion($value);
        $this->assertEquals($value,$CMaterielTest->getVersion());

    }

    /**
     * Test de la methode setRef
     * @depends testGetRef
     */
    public function testSetRef(){
        $CMaterielTest= new CMateriel("Samsung","SAMG10",1234,"portable",0,"www.google.com/image/","0767473970");
        $value=667;
        $CMaterielTest->setRef($value);
        $this->assertEquals($value,$CMaterielTest->getRef());

    }

     /**
     * Test de la methode setType
     * @depends testGetType
     */
    public function testSetType(){
        $CMaterielTest=  new CMateriel("Samsung","SAMG10",1234,"portable",0,"www.google.com/image/","0767473970");
        $value="portable";
        $CMaterielTest->setType($value);
        $this->assertEquals($value,$CMaterielTest->getType());

    }

     /**
     * Test de la methode setTel
     * @depends testGetTel
     */
    public function testSetTel(){
        $CMaterielTest=  new CMateriel("Samsung","SAMG10",1234,"portable",0,"www.google.com/image/","0767473970");
        $value="0767473970";
        $CMaterielTest->setNumTel($value);
        $this->assertEquals($value,$CMaterielTest->getNumTel());

    }

     /**
     * Test de la methode setPhoto
     * @depends testGetPhoto
     */
    public function testSetPhoto()
    {
        $CMaterielTest = new CMateriel("Samsung", "SAMG10", 1234, "portable", 0, "www.google.com/image/", "0767473970");
        $value = "www.yahoo.com/image/";
        $CMaterielTest->setPhoto($value);
        $this->assertEquals($value, $CMaterielTest->getPhoto());

    }

}

?>