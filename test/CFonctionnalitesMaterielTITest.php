<?php

//import
use PHPUnit\Framework\TestCase;
require_once('../Controleur/CFonctionnalitesMateriel.class.php');
require_once("../Controleur/CFonctionnalitesMateriel.class.php");

/**
 * Classe de test de la classe CFonctionnalitesMateriel
 * @author Mael Pons & Honorine Chantre
 */
class CFonctionnalitesMaterielTITest extends TestCase
{
    /**
     * Test la consultation d un matériel dans la bdd
     */
    public function testConsulterMateriels()
    {
        $pDBB= new CBdd();

        try
        {
            $pDBB->creer_bdd();
        } catch(Exception $e){
            die('connexion echouee : '.$e->getMessage())."<br/>";
        }
        $materielTest= new CMateriel("MacBook Air","MacOS 10.14","AP001","Ordinateur",0,NULL,NULL);
        $fctMateriel= new CFonctionnalitesMateriel($materielTest);
        $fctMateriel->creerMateriel();
        $consultation=$fctMateriel->consulterMateriels($materielTest->getRef());
        $this->assertEquals($materielTest->getNom(),$consultation[0]['nom']);
        $this->assertEquals($materielTest->getType(),$consultation[0]['type']);
        $this->assertEquals($materielTest->getRef(),$consultation[0]['reference']);
        $this->assertEquals($materielTest->getVersion(),$consultation[0]['version']);
        $this->assertEquals($materielTest->getPhoto(),$consultation[0]['photo']);
        $this->assertEquals($materielTest->getType(),$consultation[0]['type']);
        $this->assertEquals($materielTest->getStatut(),$consultation[0]['statut']);
        $fctMateriel->supprimerMateriel("AP001");

    }

    /**
     * Test la création d'un material dans la bdd
     * @depends testConsulterMateriels
     */
    public function testCreerMateriel()
    {
        $pDBB= new CBdd();

        try
        {
            $pDBB->creer_bdd();
        } catch(Exception $e){
            die('connexion echouee : '.$e->getMessage())."<br/>";
        }
        $materielTest= new CMateriel("Samsung","SAMG10",1234,"portable",0,"www.google.com/image/","0767473970");
        $fctMateriel= new CFonctionnalitesMateriel($materielTest);
        $fctMateriel->creerMateriel();
        $consultation=$fctMateriel->consulterDetailMateriel($materielTest->getRef());
        $this->assertTrue($materielTest->getNom()==$consultation['nom']);
        $this->assertTrue($materielTest->getType()==$consultation['type']);
        $this->assertTrue($materielTest->getRef()==$consultation['reference']);
        $this->assertTrue($materielTest->getVersion()==$consultation['version']);
        $this->assertTrue($materielTest->getPhoto()==$consultation['photo']);
        $this->assertTrue($materielTest->getType()==$consultation['type']);
        $this->assertEquals($materielTest->getStatut(),$consultation['statut']);
        $fctMateriel->supprimerMateriel(1234);

    }

    /**
     * Test la modification d'un materiel dans la bdd
     */
    public function testModifierMateriel()
    {
        $pDBB= new CBdd();

        try
        {
            $pDBB->creer_bdd();
        } catch(Exception $e){
            die('connexion echouee : '.$e->getMessage())."<br/>";
        }
        $materielTest= new CMateriel("Samsung","SAMG10",1234,"portable",0,"www.google.com/image/","0767473970");
        $materielTest2= new CMateriel("iphone","123",1234,"portable",0,"www.google.com/image/","0767488970");
        $fctMateriel= new CFonctionnalitesMateriel($materielTest);
        $fctMateriel->creerMateriel();
        $fctMateriel->setMateriel($materielTest2);
        $fctMateriel->modifierMateriel();
        $consultation=$fctMateriel->consulterDetailMateriel($materielTest->getRef());
        $this->assertEquals($materielTest2->getNom(),$consultation['nom']);
        $this->assertTrue($materielTest2->getType()==$consultation['type']);
        $this->assertTrue($materielTest2->getRef()==$consultation['reference']);
        $this->assertTrue($materielTest2->getVersion()==$consultation['version']);
        $this->assertTrue($materielTest2->getPhoto()==$consultation['photo']);
        $this->assertTrue($materielTest2->getType()==$consultation['type']);
        $this->assertTrue($materielTest2->getStatut()==$consultation['statut']);
        $fctMateriel->supprimerMateriel(1234);

    }

    /**
     * Test modification de statut d'un materiel dans la bdd
     */
    public function testRendreDisponibleMateriel()
    {
        $pDBB= new CBdd();

        try
        {
            $pDBB->creer_bdd();
        } catch(Exception $e){
            die('connexion echouee : '.$e->getMessage())."<br/>";
        }
        $materielTest= new CMateriel("Samsung","SAMG10",1234,"portable",0,"www.google.com/image/","0767473970");
        $fctMateriel= new CFonctionnalitesMateriel($materielTest);
        $fctMateriel->creerMateriel();
        $fctMateriel->rendreDisponibleMateriel($materielTest->getRef());
        $consultation=$fctMateriel->consulterDetailMateriel($materielTest->getRef());
        $this->assertEquals(true, $consultation['statut']==1);

    }

    /**
     * Test le setter d'un materiel dans l'objet CfonctionnalitesMateriel
     */
    public function testSetMateriel()
    {
        $pDBB= new CBdd();

        try
        {
            $pDBB->creer_bdd();
        } catch(Exception $e){
            die('connexion echouee : '.$e->getMessage())."<br/>";
        }
        $materielTest= new CMateriel("Samsung","SAMG10",1234,"portable",0,"www.google.com/image/","0767473970");
        $fctMateriel= new CFonctionnalitesMateriel($materielTest);
        $fctMateriel->creerMateriel();
        $this->assertEquals($materielTest, $fctMateriel->getMateriel());
        $fctMateriel->supprimerMateriel(1234);

    }

    /**
     * Test la consultation detaillee d'un materiel dans la bdd
     */
    public function testConsulterDetailMateriel()
    {
        $pDBB= new CBdd();

        try
        {
            $pDBB->creer_bdd();
        } catch(Exception $e){
            die('connexion echouee : '.$e->getMessage())."<br/>";
        }
        $materielTest= new CMateriel("Samsung","SAMG10",1234,"portable",0,"www.google.com/image/","0767473970");
        $fctMateriel= new CFonctionnalitesMateriel($materielTest);
        $fctMateriel->creerMateriel();
        $consultation=$fctMateriel->consulterDetailMateriel($materielTest->getRef());
        $this->assertEquals($materielTest->getNom(),$consultation['nom']);
        $this->assertTrue($materielTest->getType()==$consultation['type']);
        $this->assertTrue($materielTest->getRef()==$consultation['reference']);
        $this->assertTrue($materielTest->getVersion()==$consultation['version']);
        $this->assertTrue($materielTest->getPhoto()==$consultation['photo']);
        $this->assertTrue($materielTest->getType()==$consultation['type']);
        $this->assertTrue($materielTest->getStatut()==$consultation['statut']);
        $fctMateriel->supprimerMateriel(1234);

    }

    /**
     * Test la suppression d'un materiel dans la bdd
     */
    public function testSupprimerMateriel()
    {
        $pDBB= new CBdd();

        try
        {
            $pDBB->creer_bdd();
        } catch(Exception $e){
            die('connexion echouee : '.$e->getMessage())."<br/>";
        }
        $materielTest= new CMateriel("Samsung","SAMG10",1234,"portable",0,"www.google.com/image/","0767473970");
        $fctMateriel= new CFonctionnalitesMateriel($materielTest);
        $fctMateriel->creerMateriel();
        $fctMateriel->supprimerMateriel(1234);
        $sql = "SELECT COUNT(*) FROM Materiel";
        $result=$fctMateriel->getBdd()->lire($sql);
        $this->assertEquals(NULL,$result==NULL);

    }
}
