<?php

// Imports
require_once('CBdd.class.php');

/**
 * Classe representant les fonctionnalites appliquees a une recherche d'un materiel
 */
class RechercheMateriel {

    /**
     * Attribut utilise pour la connexion a la base de donnees
     */
    private $bdd;

    /**
     * Constructeur de la classe 
     */
    public function __construct(){
        $this->bdd = new CBdd();
        $this->bdd->connecter_bdd();
    }

    /**
     * Getter de l'attribut bdd
     * @return bdd la bdd
     */
    public function getBdd(){
        return $this->bdd;
    }

    /**
     * Setter de l'attribut bdd
     * @param bdd une bdd
     */
    public function setBdd($bddParam){
        $this->bdd = $bddParam;
    }

    /**
     * Methode permettant de rechercher les materiels en fonction de leur type
     * @param type le type des materiels
     */
    public function RechercheType($type){
        
        try {
            // Consultation des informations d'un utilisateur de la table Utilisateur
            $sql = "SELECT m.ref_materiel, version_materiel, nom_materiel, photo_materiel, type_materiel, num_tel_materiel, 
                    statut_materiel, fin_emprunt FROM Materiel m LEFT JOIN Emprunter e ON m.ref_materiel = e.ref_materiel 
                    WHERE type_materiel ="."'".$type."'";
            $result = $this->bdd->lire($sql);

            while($row = $result->fetch()) {
                $detail = array(
                    'nom' => $row['nom_materiel'], 
                    'version' => $row['version_materiel'],
                    'reference' => $row['ref_materiel'],
                    'photo' => $row['photo_materiel'],
                    'type' => $row['type_materiel'],  
                    'statut' => $row['statut_materiel'],
                    'dateFin' => $row['fin_emprunt'],
                );
                
                $tousLesMat [] = $detail;
            }
            
        } catch(Exception $e) {
            // En cas d'erreur, on affiche un message et on arrete tout
            die('connexion échouée : '.$e->getMessage())."<br/>";

        }

        return $tousLesMat;

    }

    /**
     * Methode permettant de rechercher les materiels en fonction de leur statut
     * @param statut le statut des materiels
     */
    public function RechercheDisponibilite($statut){
        
        try {
            // Consultation des informations d'un utilisateur de la table Utilisateur
            $sql = "SELECT m.ref_materiel, version_materiel, nom_materiel, photo_materiel, type_materiel, num_tel_materiel, 
                    statut_materiel, fin_emprunt FROM Materiel m LEFT JOIN Emprunter e ON m.ref_materiel = e.ref_materiel 
                    WHERE statut_materiel ="."'".$statut."'";
            $result = $this->bdd->lire($sql);

            while($row = $result->fetch()) {
                $detail = array(
                    'nom' => $row['nom_materiel'], 
                    'version' => $row['version_materiel'],
                    'reference' => $row['ref_materiel'],
                    'photo' => $row['photo_materiel'],
                    'type' => $row['type_materiel'],  
                    'statut' => $row['statut_materiel'],
                    'dateFin' => $row['fin_emprunt'],
                );
                
                $tousLesMat [] = $detail;
            }
            
        } catch(Exception $e) {
            // En cas d'erreur, on affiche un message et on arrete tout
            die('connexion échouée : '.$e->getMessage())."<br/>";

        }

        return $tousLesMat;

    }


    /**
     * Methode permettant de rechercher les materiels en fonction de ce que tape l'utilisateur
     * @param critere le statut des materiels
     */
    public function Recherche($critere){
        
        try {
            // Consultation des informations d'un utilisateur de la table Utilisateur
            $sql = "SELECT m.ref_materiel, version_materiel, nom_materiel, photo_materiel, type_materiel, num_tel_materiel, 
                    statut_materiel, fin_emprunt FROM Materiel m LEFT JOIN Emprunter e ON m.ref_materiel = e.ref_materiel 
                    WHERE m.ref_materiel LIKE"."'%".$critere."%'"."OR nom_materiel LIKE"."'%".$critere."%'".
                    "OR type_materiel LIKE"."'%".$critere."%'"."OR version_materiel LIKE"."'%".$critere."%'";
            $result = $this->bdd->lire($sql);

            while($row = $result->fetch()) {
                $detail = array(
                    'nom' => $row['nom_materiel'], 
                    'version' => $row['version_materiel'],
                    'reference' => $row['ref_materiel'],
                    'photo' => $row['photo_materiel'],
                    'type' => $row['type_materiel'],  
                    'statut' => $row['statut_materiel'],
                    'dateFin' => $row['fin_emprunt'],
                );
                
                $tousLesMat [] = $detail;
            }
            
        } catch(Exception $e) {
            // En cas d'erreur, on affiche un message et on arrete tout
            die('connexion échouée : '.$e->getMessage())."<br/>";

        }

        return $tousLesMat;

    }
        
 }
?>