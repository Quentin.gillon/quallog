<?php

/**
 * Classe permettant la connexion et la creation de la base de donnees
 * @author Manon Cottart
 */
class CBdd {

    /**
     * Attribut utilise pour la connexion a la base de donnees
     */
    private $bdd;

    /**
     * Attribut utilise pour la connexion au SGBD
     */
    private $sgbd;

    public function getBdd(){
        return $this->bdd;
    }

    /**
     * Methode permettant la connexion au SGBD
     * @return sgbd la connexion 
     * @throws Exception
     */
	public function connecter_SGBD() {
		try
		{
            $this->sgbd = new PDO('mysql:host=localhost;charset=UTF8', 'root', '');
		} catch(Exception $e)

		{
			// En cas d'erreur, on affiche un message et on arrete tout
        	die('connexion echouee : '.$e->getMessage())."<br/>";
		}
		return $this->sgbd;
    }

    /**
     * Methode permettant la connexion a la base de données
     * @return bdd la connexion
     * @throws Exception
     */
    public function connecter_bdd() {
		try
		{
           $this->bdd = new PDO('mysql:host=localhost;dbname=rentamat;charset=UTF8', 'root', '');
		} catch(Exception $e)
		{
			// En cas d'erreur, on affiche un message et on arrete tout
        	die('connexion echouee : '.$e->getMessage())."<br/>";
		}
		return $this->bdd;
    }
    
    /**
     * Methode permettant la creation de la base de donnees et de ses tables
     * @return result un booleen selon si ca a ete effectue ou non
     * @throws Exception
     */
    public function creer_bdd() {
        try
        {
            // Creation de la base de donnees et connexion au SGBD
            $sgbd = $this->connecter_SGBD();
            $sql = "CREATE DATABASE IF NOT EXISTS RentAMat";
            $prep = $this->sgbd->prepare($sql);
            $prep->execute();
        
            // Connexion a la base de donnes RentAMat
            $bdd = $this->connecter_bdd();
            
            // Creation de la table Utilisateur
            $sql = "CREATE TABLE IF NOT EXISTS Utilisateur 
                    (matricule_utilisateur CHAR(7) PRIMARY KEY NOT NULL, 
                    nom_utilisateur VARCHAR(30) NOT NULL, 
                    prenom_utilisateur VARCHAR(30) NOT NULL, 
                    mdp_utilisateur VARCHAR(20) NOT NULL, 
                    role_utilisateur VARCHAR(20) NOT NULL, 
                    email_utilisateur VARCHAR(320) NOT NULL) ENGINE=INNODB";
            $prep = $this->bdd->prepare($sql);
            $prep->execute();

            // Creation de la table Materiel
            $sql = "CREATE TABLE IF NOT EXISTS Materiel 
                    (ref_materiel CHAR(5) PRIMARY KEY NOT NULL, 
                    version_materiel CHAR(15) NOT NULL, 
                    nom_materiel CHAR(30) NOT NULL, 
                    photo_materiel VARCHAR(256), 
                    type_materiel CHAR(10) NOT NULL, 
                    num_tel_materiel CHAR(10),
                    statut_materiel BOOLEAN NOT NULL) ENGINE=INNODB";
            $prep = $this->bdd->prepare($sql);
            $prep->execute();

            // Creation de la table Reserver
            $sql = "CREATE TABLE IF NOT EXISTS Reserver 
                    (ref_materiel CHAR(5) NOT NULL, 
                    matricule_utilisateur CHAR(7) NOT NULL, 
                    debut_reservation DATE NOT NULL, 
                    fin_reservation DATE NOT NULL, 
                    FOREIGN KEY (ref_materiel) REFERENCES Materiel(ref_materiel),
                    FOREIGN KEY (matricule_utilisateur) REFERENCES Utilisateur(matricule_utilisateur),
                    PRIMARY KEY (ref_materiel,matricule_utilisateur,debut_reservation)) ENGINE=INNODB";
            $prep = $this->bdd->prepare($sql);
            $prep->execute();
            
            // Creation de la table Emprunter
            $sql = "CREATE TABLE IF NOT EXISTS Emprunter 
                    (ref_materiel CHAR(5) NOT NULL, 
                    matricule_utilisateur CHAR(7) NOT NULL, 
                    debut_emprunt DATE NOT NULL, 
                    fin_emprunt DATE NOT NULL, 
                    FOREIGN KEY (ref_materiel) REFERENCES Materiel(ref_materiel),
                    FOREIGN KEY (matricule_utilisateur) REFERENCES Utilisateur(matricule_utilisateur),
                    PRIMARY KEY (ref_materiel,matricule_utilisateur,debut_emprunt)) ENGINE=INNODB";
            $prep = $this->bdd->prepare($sql);
            $prep->execute();

            $result = $bdd;
        } catch(Exception $e) {
            // En cas d'erreur, on affiche un message et on arrete tout
            die('connexion échouée : '.$e->getMessage())."<br/>";
        }
        return $result;
    }

    /**
     * Methode permettant de preparer une requete
	* @param req une requete 
	* @return res le résultat de la preparation de la requete
	*/
    public function preparer_requete($req){
		$res = $this->bdd->prepare($req);
		return $res;
    }

    /**
     * Methode permettant de lire dans la base de donnees
     * @param req une requete de type "SELECT * FROM table"
     * @return res le résultat de la requête sous forme d'un objet
     */
    public function lire($req){
        $res = $this->bdd->query($req);
        return $res;
    }



    /**
    * Methode permettant de lire dans la base de donnees pour l'authentification
	* @param requete une requete de type "SELECT * FROM table"
    * @return resultat le résultat de la requête sous forme de tableau
	*/
	public function lire_authentification($requete){
        $req = $this->bdd->prepare($requete);
        $resultat = $req->execute();
        $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
        return $resultat;
	}

}
?>