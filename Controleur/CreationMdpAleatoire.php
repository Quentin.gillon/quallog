<?php

/**
 * Fonction permettant de generer un mot de passe aleatoire
 * @return mdp un mot de passe genere aleatoirement
 */
function creerMdpAleatoire(){
    $caracteres = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
    $mdp = '';
    for($i=0;$i<20;$i++)
    {
        $mdp .= ($i%2) ? strtoupper($caracteres[array_rand($caracteres)]) : $caracteres[array_rand($caracteres)];
    }
		
    return $mdp;
}

?>