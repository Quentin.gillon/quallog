<?php
    //Import
    require_once('../Controleur/CFonctionnalitesMateriel.class.php');
    $formulaireEstOK = true;    
    // Verification du nom
    if (strlen($_POST['nom']) < 1 or strlen($_POST['nom'] > 30)) {
        echo "<script>
            alert('Nom non valide. Veuillez rentrer une nom entre 1 et 30 caractères alphanumériques.');
            window.location.href = '../Vue/ConsultationMateriels.php';
        </script>";
        $formulaireEstOK = false;
    }
    // Verification de la version
    if (strlen($_POST['version']) < 3 or strlen($_POST['version'] > 15)) {
        echo "<script>
            alert('Version non valide. Veuillez rentrer une version entre 1 et 15 caractères alphanumériques.');
            window.location.href = '../Vue/ConsultationMateriels.php';
        </script>";
        $formulaireEstOK = false;
    }
    // Verification du numero de téléphone
    
    if (strlen($_POST['tel']) != 0 and strlen($_POST['tel']) != 10)  {
        echo "<script>
            alert('Numero de telephone non valide. Veuillez rentrer un numero de 10 caractères alphanumériques.');
            window.location.href = '../Vue/ConsultationMateriels.php';
        </script>";
        $formulaireEstOK = false;
    }
    if($formulaireEstOK){
        if($_POST['disponibilite'] == 'Oui'){
            $bool = 1;
        }
        if($_POST['disponibilite'] == 'Non'){
            $bool = 0;
        }
        $mat1 = new CMateriel($_POST['nom'], $_POST['version'], $_GET['reference'], "portable",$bool, $_POST['photo'], $_POST['tel']);
        $matcree1 = new CFonctionnalitesMateriel($mat1);
        $matcree1->modifierMateriel();
        header('Location: ../vue/ConsultationMateriels');
    }
?>
