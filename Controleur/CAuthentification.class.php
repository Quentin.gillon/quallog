<?php

require_once ('CBdd.class.php');
require_once ('../vue/index.php');

/**
 * Classe permettant la connexion et la deconnexion de l'application
 * @author Damien Lemer et Honorine Chantre
 */
class CAuthentification {

    /**
     * Attribut utilise pour l'email lors de l'authentification
     */
	private  $Email;
    /**
     * Attribut utilise pour le mot de passe lors de l'authentification
     */
	private  $Mot_de_passe;
    /**
     * Attribut utilise pour la connexion a la base de donnees
     */
	private  $pBDD;

    /**
     * Constructeur de la classe
     */
	public function __construct($email, $mot_de_passe) {
		$this->pBDD = new CBdd();
		$this->pBDD->connecter_bdd();
		$this->Email = $email;
		$this->Mot_de_passe = $mot_de_passe;
	}

    /**
     * Methode permettant l'authentification au site web
     * @throws Exception
     */
	public function authentifier() {
        try {
            $resultat = $this->pBDD->lire_authentification("SELECT * FROM utilisateur WHERE email_utilisateur = " . "'" . $this->Email . "'" . "AND mdp_utilisateur = " . "'" . $this->Mot_de_passe . "'");
            $number_res = count($resultat);
            if ($number_res === 1) {
                session_start();  // démarrage d'une session
                // l'utilisateur existe dans la table
                // on ajoute ses infos en tant que variables de session
                $_SESSION['email'] = $this->Email;
                $_SESSION['mdp'] = $this->Mot_de_passe;
                foreach($resultat as $utilisateur){
					$_SESSION['matricule'] = $utilisateur['matricule_utilisateur'];
					$_SESSION['role'] = $utilisateur['role_utilisateur'];
				}
				if ($_SESSION['role'] == 'Administrateur') {
					header('location: ../vue/ConsultationMateriels.php');      
				  }else{
					header('location: ../vue/ConsultationMaterielsEmprunteur.php');
				  }
            } else {
                echo '<script>
					alert("Vous avez renseigne une mauvaise adresse mail et/ou un mauvais mot de passe" );
					window.location.href = "../vue/index.php";
				</script>';
                exit();
            }
        }catch (Exception $e){
            // En cas d'erreur, on affiche un message et on arrete tout
            die('connexion échouée : '.$e->getMessage())."<br/>";
        }
	}

    /**
     * Methode permettant la deconnexion au site web
     * @throws Exception
     */
	public function se_deconnecter() {
		// On démarre la session
		session_start ();

		// On détruit les variables de notre session
		session_unset ();
		
		// On détruit notre session
		session_destroy ();
		
		// On redirige le visiteur vers la page d'accueil
		header ('location: ../vue/index.php');
	}


}
?>