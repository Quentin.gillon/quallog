<?php
    // Import
    require_once('../Controleur/CFonctionnalitesMateriel.class.php');
    $formulaireEstOK = true;    
    // Verification du nom
    if (strlen($_POST['nom']) < 1 or strlen($_POST['nom'] > 30)) {
        echo "<script>
            alert('Nom non valide. Veuillez rentrer une nom entre 1 et 30 caractères alphanumériques.');
            window.location.href = '../Vue/CreationMateriel.php';
        </script>";
        $formulaireEstOK = false;
    }
    // Verification de la version
    if (strlen($_POST['version']) < 3 or strlen($_POST['version'] > 15)) {
        echo "<script>
            alert('Version non valide. Veuillez rentrer une version entre 3 et 15 caractères alphanumériques.');
            window.location.href = '../Vue/CreationMateriel.php';
        </script>";
        $formulaireEstOK = false;
    }
    // Verification de la référence
    $ref = $_POST['reference'];
    if (!preg_match(" /(AN|AP|XX)([0-9]{3})/ ", $ref)) {
        echo "<script>
            alert('Référence non valide. Veuillez rentrer une référence commençant par AN ou AP ou XX suivi de 3 chiffres.');
            window.location.href = '../Vue/CreationMateriel.php';
        </script>";
        $formulaireEstOK = false;
    }
    // Verification du numero de téléphone
    if (strlen($_POST['tel']) != 0 and strlen($_POST['tel']) != 10)  {
        echo "<script>
            alert('Numero de telephone non valide. Veuillez rentrer un numero de 10 caractères alphanumériques.');
            window.location.href = '../Vue/CreationMateriel.php';
        </script>";
        $formulaireEstOK = false;
    }
    if($formulaireEstOK){
        //Création d'un materiel dans la base de donnée
        if($_POST['disponibilite'] == 'Oui'){
            $bool = 1;
        }
        if($_POST['disponibilite'] == 'Non'){
            $bool = 0;
        }    
        $mat1 = new CMateriel($_POST['nom'],$_POST['version'],$_POST['reference'],$_POST['type'],$bool,$_POST['photo'],$_POST['numero']);
        $matcree1 = new CFonctionnalitesMateriel($mat1);
        $matcree1->creerMateriel();
        //Redirection vers la bonne page
        header('Location: ../vue/ConsultationMateriels');
    }
?>