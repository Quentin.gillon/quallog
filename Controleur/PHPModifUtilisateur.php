<?php
    // Import
    require_once('../Controleur/CFonctionnalitesUtilisateur.class.php');
    $formulaireEstOK = true;
    // Verification de l'adresse mail
    if (!filter_var($_POST['adresseMail'], FILTER_VALIDATE_EMAIL)) {
        echo "<script>
            alert('Adresse mail non valide. Veuillez rentrer une adresse mail valide.');
            window.location.href = '../Vue/CreationUtilisateur.php';
        </script>";
        $formulaireEstOK = false;
    }
    // Verification du nom
    if (strlen($_POST['nom']) > 30 or  strlen($_POST['nom']) < 1) {
        echo "<script>
            alert('Nom non valide. Veuillez rentrer un nom entre 1 et 30 caractères alphanumériques.');
            window.location.href = '../Vue/CreationUtilisateur.php';
        </script>";
        $formulaireEstOK = false;
    }
    // Verification du prenom
    if (strlen($_POST['prenom']) > 30 or  strlen($_POST['prenom']) < 1) {
        echo "<script>
            alert('Prenom non valide. Veuillez rentrer un prenom entre 1 et 30 caractères alphanumériques.');
            window.location.href = '../Vue/CreationUtilisateur.php';
        </script>";
        $formulaireEstOK = false;
    }
    $user1 = new CUtilisateur($_GET['matricule'], $_POST['nom'], $_POST['prenom'], $_POST['role'], $_POST['mail']);   
    $usercree1 = new CFonctionnalitesUtilisateur($user1);
    $usercree1->modifierUtilisateur();
    //Redirection vers la bonne page
    header('Location: ../vue/ConsultationDetailUtilisateur?matricule=' . $_GET['matricule']);
?>

