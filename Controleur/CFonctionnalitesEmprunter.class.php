<?php

// Imports
require_once('../Modele/CEmprunter.class.php');
require_once('CBdd.class.php');

/**
 * Classe representant les fonctionnalites appliquees a un emprunt
 * @author Manon Cottart
 */
class CFonctionnalitesEmprunter {

    /**
     * Attribut de type CEmprunter
     */
    private $emprunt;

    /**
     * Attribut utilise pour la connexion a la base de donnees
     */
    private $bdd;

    /**
     * Constructeur de la classe 
     */
    public function __construct($empruntParam){
        $this->emprunt = $empruntParam;
        $this->bdd = new CBdd();
        $this->bdd->connecter_bdd();
    }

    /**
     * Getter de l'attribut emprunt
     * @return emprunt l'emprunt
     */ 
    public function getEmprunt()
    {
        return $this->emprunt;
    }

    /**
     * Getter de l'attribut bdd
     * @return bdd la bdd
     */
    public function getBdd(){
        return $this->bdd;
    }

    /**
     * Setter de l'attribut emprunt
     * @param empruntParam un emprunt
     */ 
    public function setEmprunt($empruntParam)
    {
        $this->emprunt = $empruntParam;
    }

    /**
     * Setter de l'attribut bdd
     * @param bdd une bdd
     */
    public function setBdd($bddParam){
        $this->bdd = $bddParam;
    }

    /**
     * Methode permettant d'emprunter un materiel (creer un emprunt dans la table Emprunter)
     */
    public function emprunter(){

        try {
            if ($this->emprunt->getFin_emprunt() < $this->emprunt->getDebut_emprunt()){
                echo "<script>
                    alert('La date de fin ne peut pas etre anterieure a aujourd'hui ! Veuillez reessayer.');
                    window.location.href = '../Vue/Emprunter.php';
                </script>";
            }

            // Obtention du statut du materiel voulant etre emprunte
            $sql = "SELECT statut_materiel FROM Materiel WHERE ref_materiel = "."'".$this->emprunt->getRef()."'";
            $result = $this->bdd->lire($sql);
            $row = $result->fetch();
            $statut = $row['statut_materiel'];

            // Si le materiel est disponible
            if ($statut == 1) {
                // Creation d'un emprunt dans la table Emprunter
                $sql1 = "INSERT INTO Emprunter (ref_materiel, matricule_utilisateur, 
                                                debut_emprunt, fin_emprunt) 
                        VALUES (:ref, :matricule, :debemprunt, :finemprunt)";
                $prep1 = $this->bdd->preparer_requete($sql1); 
                $prep1->execute(array(
                    'ref' => $this->emprunt->getRef(),
                    'matricule' => $this->emprunt->getMatricule(), 
                    'debemprunt' => $this->emprunt->getDebut_emprunt(),
                    'finemprunt' => $this->emprunt->getFin_emprunt(),
                ));

                $sql2 = "UPDATE Materiel
                SET statut_materiel = 0
                WHERE ref_materiel = :ref";
                $prep2 = $this->bdd->preparer_requete($sql2); 
                $prep2->execute(array(
                    'ref' => $this->emprunt->getRef(),
                ));        
            }

            // Si le materiel n'est pas disponible
            elseif ($statut == 0) {
                echo "<script>
                    alert('Le materiel n'est pas disponible pour le moment mais vous pouvez le reserver !');
                    window.location.href = '../Vue/ConsultationDetailMaterielEmprunteur.php';
                </script>";
            }

        } catch(Exception $e) {
            // En cas d'erreur, on affiche un message et on arrete tout
            die('connexion échouée : '.$e->getMessage())."<br/>";
        }
    }

    /**
     * Methode permettant de consulter l'historique des emprunts des materiels
     * @return historique un tableau contenant l'historique des emprunts
     */
    public function consulterHistoriqueEmprunts(){

        try {

            // Consultation de l'historique des emprunts
            $sql = "SELECT nom_utilisateur, prenom_utilisateur, matricule_utilisateur, 
                    nom_materiel, type_materiel, ref_materiel,
                    debut_emprunt, fin_emprunt 
                    FROM Materiel m 
                    JOIN Emprunter e ON m.ref_materiel = e.ref_materiel
                    JOIN Utilisateur u ON u.matricule_utilisateur = e.matricule_utilisateur";
            
            $result = $this->bdd->lire($sql);

            while($row = $result->fetch()) {
                $details = array(
                    'nomU' => $row['nom_utilisateur'], 
                    'prenomU' => $row['prenom_utilisateur'],
                    'matriculeU' => $row['matricule_utilisateur'],
                    'nomM' => $row['nom_materiel'],
                    'typeM' => $row['type_materiel'],  
                    'refM' => $row['ref_materiel'],
                    'debutEmprunt' => $row['debut_emprunt'],
                    'finEmprunt' => $row['fin_emprunt'],
                );
                
                $historique [] = $details;
            }
            
        } catch(Exception $e) {
            // En cas d'erreur, on affiche un message et on arrete tout
            die('connexion échouée : '.$e->getMessage())."<br/>";

        }

        return $historique;
    }
}
?>