<?php

// Imports
require_once('CBdd.class.php');

/**
 * Classe representant les fonctionnalites appliquees a la recherche d'un utilisateur
 */
class RechercheUtilisateur {

    /**
     * Attribut utilise pour la connexion a la base de donnees
     */
    private $bdd;

    /**
     * Constructeur de la classe 
     */
    public function __construct(){
        $this->bdd = new CBdd();
        $this->bdd->connecter_bdd();
    }

    /**
     * Getter de l'attribut bdd
     * @return bdd la bdd
     */
    public function getBdd(){
        return $this->bdd;
    }

    /**
     * Setter de l'attribut bdd
     * @param bdd une bdd
     */
    public function setBdd($bddParam){
        $this->bdd = $bddParam;
    }

    /**
     * Methode permettant de rechercher les utilisateurs en fonction de leur role
     * @param type le type des materiels
     */
    public function RechercheRole($role){
        
        try {
            // Consultation des informations d'un utilisateur de la table Utilisateur
            $sql = "SELECT * FROM Utilisateur WHERE role_utilisateur ="."'".$role."'";
            $result = $this->bdd->lire($sql);

            while($row = $result->fetch()) {
                $detail = array(
                    'nom' => $row['nom_utilisateur'], 
                    'prenom' => $row['prenom_utilisateur'],
                    'email' => $row['email_utilisateur'],
                    'matricule' => $row['matricule_utilisateur'],
                    'role' => $row['role_utilisateur'],  
                );

                $tousLesUtil [] = $detail;
            }
            
        } catch(Exception $e) {
            // En cas d'erreur, on affiche un message et on arrete tout
            die('connexion échouée : '.$e->getMessage())."<br/>";
        }

        return $tousLesUtil;
    }
   

    /**
     * Methode permettant de rechercher les utilisateurs en fonction de ce que tape l'administrateur
     * @param critere le statut des materiels
     */
    public function Recherche($critere){
        
        try {
            // Consultation des informations d'un utilisateur de la table Utilisateur
            $sql = "SELECT * FROM Utilisateur WHERE nom_utilisateur LIKE"."'%".$critere."%'"."OR prenom_utilisateur LIKE"."'%".$critere."%'".
                    "OR matricule_utilisateur LIKE"."'%".$critere."%'"."OR email_utilisateur LIKE"."'%".$critere."%'".
                    "OR role_utilisateur LIKE"."'%".$critere."%'";
            $result = $this->bdd->lire($sql);

            while($row = $result->fetch()) {
                $detail = array(
                    'nom' => $row['nom_utilisateur'], 
                    'prenom' => $row['prenom_utilisateur'],
                    'email' => $row['email_utilisateur'],
                    'matricule' => $row['matricule_utilisateur'],
                    'role' => $row['role_utilisateur'],  
                );

                $tousLesUtil [] = $detail;
            }
            
        } catch(Exception $e) {
            // En cas d'erreur, on affiche un message et on arrete tout
            die('connexion échouée : '.$e->getMessage())."<br/>";
        }

        return $tousLesUtil;
    }
        
 }
?>