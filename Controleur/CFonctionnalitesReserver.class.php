<?php

// Imports
require_once('../Modele/CReserver.class.php');
require_once('CBdd.class.php');

/**
 * Classe representant les fonctionnalites appliquees a une reservation
 * @author Manon Cottart
 */
class CFonctionnalitesReserver {

    /**
     * Attribut de type CReserver
     */
    private $reservation;

    /**
     * Attribut utilise pour la connexion a la base de donnees
     */
    private $bdd;

    /**
     * Constructeur de la classe 
     */
    public function __construct($reservationParam){
        $this->reservation = $reservationParam;
        $this->bdd = new CBdd();
        $this->bdd->connecter_bdd();
    }

    /**
     * Getter de l'attribut reservation
     * @return reservation la reservation
     */ 
    public function getReservation()
    {
        return $this->reservation;
    }

    /**
     * Getter de l'attribut bdd
     * @return bdd la bdd
     */
    public function getBdd(){
        return $this->bdd;
    }

    /**
     * Setter de l'attribut reservation
     * @param reservationParam une reservation
     */ 
    public function setReservation($reservationParam)
    {
        $this->reservation = $reservationParam;
    }

    /**
     * Setter de l'attribut bdd
     * @param bdd une bdd
     */
    public function setBdd($bddParam){
        $this->bdd = $bddParam;
    }

    /**
     * Methode permettant de reserver un materiel (creer une reservation dans la table Reserver)
     * @throws Exception
     */
    public function reserver(){

        try {
            if ($this->reservation->getFin_reservation() < $this->reservation->getDebut_reservation()){
                echo "<script>
                    alert('La date de fin ne peut pas etre anterieure a la date de debut ! Veuillez reessayer.');
                    window.location.href = '../Vue/Reservation.php';
                </script>";
            } else {

                // Obtention du statut du materiel voulant etre reserve
                $sql = "SELECT statut_materiel FROM Materiel WHERE ref_materiel = "."'".$this->reservation->getRef()."'";
                $result = $this->bdd->lire($sql);
                $row = $result->fetch();
                $statut = $row['statut_materiel'];

                $sql2 = "SELECT fin_emprunt FROM Emprunter WHERE ref_materiel = "."'".$this->reservation->getRef()."'";
                $result2 = $this->bdd->lire($sql2);
                $row2 = $result2->fetch();
                $date_fin_emprunt = $row2['fin_emprunt'];

                if ($this->reservation->getDebut_reservation() < $date_fin_emprunt){
                    echo "<script>
                        alert('La date de debut de reservation ne peut pas etre avant la date de fin d'emprunt !');
                        window.location.href = '../Vue/ConsultationMateriels.php';
                    </script>";
                }
                else {
                    // Si le materiel n'est pas disponible
                    if ($statut == 0) {
                        // Creation d'une reservation dans la table Reserver
                        $sql = "INSERT INTO Reserver (ref_materiel, matricule_utilisateur, 
                                                        debut_reservation, fin_reservation) 
                                VALUES (:ref, :matricule, :debreserv, :finreserv)";
                        $prep = $this->bdd->preparer_requete($sql); 
                        $prep->execute(array(
                            'ref' => $this->reservation->getRef(),
                            'matricule' => $this->reservation->getMatricule(), 
                            'debreserv' => $this->reservation->getDebut_reservation(),
                            'finreserv' => $this->reservation->getFin_reservation(),
                        ));
                    }

                    // Si le materiel est disponible
                    elseif ($statut == 1) {
                        echo "<script>
                            alert('Le materiel est actuellement disponible. Empruntez-le !');
                            window.location.href = '../Vue/ConsultationDetailMaterielEmprunteur.php';
                        </script>";
                    }
                }
            }

        } catch(Exception $e) {
            // En cas d'erreur, on affiche un message et on arrete tout
            die('connexion échouée : '.$e->getMessage())."<br/>";
        }
    }
}
?>