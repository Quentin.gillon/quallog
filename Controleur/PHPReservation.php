<?php
    //Import
    require_once('../Controleur/CFonctionnalitesReserver.class.php');

    $reservationOK = true;

    $bdd = new CBdd();
    $bdd->creer_bdd();
    $bdd->connecter_bdd();

    $sql = "SELECT fin_emprunt FROM Emprunter WHERE ref_materiel = "."'".$_GET['reference']."'";
    $result = $bdd->lire($sql);
    $row = $result->fetch();
    $date_fin_emprunt = $row['fin_emprunt'];

    if ($this->reservation->getDebut_reservation() < $date_fin_emprunt){
        echo "<script>
            alert('La date de debut de reservation ne peut pas etre avant la date de fin d'emprunt !');
            window.location.href = '../Vue/ConsultationMateriels.php';
        </script>";
        $reservationOK = false; 
    }
    if ($reservationOK){
        $reserv = new CReserver($_GET['reference'], $_GET['matricule'], date($_POST['dateDebut']), date($_POST['dateFin'])); 
        $fonctReserv = new CFonctionnalitesReserver($reserv);
        $fonctReserv->reserver();
        header('Location: ../vue/ConsultationMateriels');
    }
?>