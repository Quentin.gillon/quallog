<?php

// Import
require_once('../Modele/CUtilisateur.class.php');
require_once('CBdd.class.php');
require_once('CreationMdpAleatoire.php');

/**
 * Classe representant les fonctionnalites appliquees a un utilisateur
 * @author Manon Cottart et Candice Ledoux
 */
class CFonctionnalitesUtilisateur {

    /**
     * Attribut de type CUtilisateur
     */
    private $utilisateur;

    /**
     * Attribut utilise pour la connexion a la base de donnees
     */
    private $bdd;

    /**
     * Constructeur de la classe
     */
    public function __construct($utilisateurParam){
        $this->utilisateur = $utilisateurParam;
        $this->bdd = new CBdd();
        $this->bdd->connecter_bdd();
    }

    /**
     * Getter de l'attribut utilisateur
     * @return utilisateur l'utilisateur
     */
    public function getUtilisateur(){
        return $this->utilisateur;
    }

    /**
     * Getter de l'attribut bdd
     * @return bdd la bdd
     */
    public function getBdd(){
        return $this->bdd;
    }

    /**
     * Methode permettant de creer un utilisateur dans la base de donnees
     * @throws Exception
     */
    public function creerUtilisateur(){
        
        try {
            // Insertion d'un utilisateur dans la table Utilisateur
            $mdp = creerMdpAleatoire();
            $sql = "INSERT INTO Utilisateur (matricule_utilisateur, nom_utilisateur, 
                                            prenom_utilisateur, mdp_utilisateur, role_utilisateur, 
                                            email_utilisateur) 
                    VALUES (:matricule, :nom, :prenom, :mdp, :role, :email)";
            $prep = $this->bdd->preparer_requete($sql); 
            if ($prep === FALSE){
                echo "false";
            }  
            $prep->execute(array(
                'matricule' => $this->utilisateur->getMatricule(),
               	'nom' => $this->utilisateur->getNom(), 
                'prenom' => $this->utilisateur->getPrenom(),
                'mdp' => $mdp,
                'role' => $this->utilisateur->getRole(),
                'email' => $this->utilisateur->getEmail(),
			));
            
        } catch(Exception $e) {
            // En cas d'erreur, on affiche un message et on arrete tout
            die('connexion échouée : '.$e->getMessage())."<br/>";

        }

    }


    /**
     * Methode permettant de modifier un utilisateur dans la base de données appliquee a un objet 
     * dont l'attribut utilisateur contient les modifs ou non a effectuer
     * @throws Exception
     */
    public function modifierUtilisateur(){

        try {
            // Modification d'un utilisateur dans la table Utilisateur
            $sql = "UPDATE Utilisateur
            SET nom_utilisateur = :nom,
                prenom_utilisateur = :prenom,
                email_utilisateur = :email,
                role_utilisateur = :role
            WHERE matricule_utilisateur = :matricule";

            $prep = $this->bdd->preparer_requete($sql); 
            $prep->execute(array(
               	'nom' => $this->utilisateur->getNom(), 
                'prenom' => $this->utilisateur->getPrenom(),
                'email' => $this->utilisateur->getEmail(),
                'role' => $this->utilisateur->getRole(),
                'matricule' => $this->utilisateur->getMatricule(),
			));

        } catch(Exception $e) {
            // En cas d'erreur, on affiche un message et on arrete tout
            die('connexion échouée : '.$e->getMessage())."<br/>";

        }
    }


    /**
     * Methode permettant de consulter un utilisateur en details
     * @param matricule un matricule
     * @return detail un tableau contenant les informations de l'utilisateur consulte
     * @throws Exception
     */
    public function consulterDetailUtilisateur($matricule){
        
        try {
            // Consultation des informations d'un utilisateur de la table Utilisateur
            $sql = "SELECT * FROM Utilisateur WHERE matricule_utilisateur ="."'".$matricule."'";
            $result = $this->bdd->lire($sql);

            $row = $result->fetch();
          
            $detail = array(
                'nom' => $row['nom_utilisateur'], 
                'prenom' => $row['prenom_utilisateur'],
                'matricule' => $row['matricule_utilisateur'],
                'email' => $row['email_utilisateur'],
                'role' => $row['role_utilisateur'],  
            );
            
        } catch(Exception $e) {
            // En cas d'erreur, on affiche un message et on arrete tout
            die('connexion échouée : '.$e->getMessage())."<br/>";
        }

        return $detail;
    }


    /**
     * Methode permettant de consulter la liste de tous les utilisateurs
     * @return tousLesUtil
     * @throws Exception
     */
    public function consulterUtilisateurs(){
        
        try {
            // Consultation des informations d'un utilisateur de la table Utilisateur
            $sql = "SELECT * FROM Utilisateur";
            $result = $this->bdd->lire($sql);

            while($row = $result->fetch()) {
                $detail = array(
                    'nom' => $row['nom_utilisateur'], 
                    'prenom' => $row['prenom_utilisateur'],
                    'email' => $row['email_utilisateur'],
                    'matricule' => $row['matricule_utilisateur'],
                    'role' => $row['role_utilisateur'],  
                );

                $tousLesUtil [] = $detail;
            }
            
        } catch(Exception $e) {
            // En cas d'erreur, on affiche un message et on arrete tout
            die('connexion échouée : '.$e->getMessage())."<br/>";
        }

        return $tousLesUtil;
    }


    /**
     * Methode permettant de modifier le mot de passe d'un utilisateur (pour la creation d'un compte
     * utilisateur pour permettre la correction du projet par les encadrants)
     * @param mdpNew nouveau mdp
     * @throws Exception
     */
    public function modifierMdp($mdpNew,$matricule){

        try {
            // Modification du mdp d'un utilisateur
            $sql = "UPDATE Utilisateur
            SET mdp_utilisateur = :mdp
            WHERE matricule_utilisateur = :matricule";
            $prep = $this->bdd->preparer_requete($sql); 
            $prep->execute(array(
               	'mdp' => $mdpNew,
                'matricule' => $matricule,
            ));
        } catch(Exception $e) {
            // En cas d'erreur, on affiche un message et on arrete tout
            die('connexion échouée : '.$e->getMessage())."<br/>";

        }
    }
   
}
?>