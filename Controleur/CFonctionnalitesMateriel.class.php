<?php

// Imports
require_once('../Modele/CMateriel.class.php');
require_once('CBdd.class.php');

/**
 * Classe representant les fonctionnalites appliquees a un materiel
 * @author Manon Cottart et Candice Ledoux
 */
class CFonctionnalitesMateriel {

    /**
     * Attribut de type CMateriel
     */
    private $materiel;

    /**
     * Attribut utilise pour la connexion a la base de donnees
     */
    private $bdd;

    /**
     * Constructeur de la classe 
     */
    public function __construct($materielParam){
        $this->materiel = $materielParam;
        $this->bdd = new CBdd();
        $this->bdd->connecter_bdd();
    }

    /**
     * Getter de l'attribut materiel
     * @return materiel le materiel
     */
    public function getMateriel(){
        return $this->materiel;
    }

    /**
     * Getter de l'attribut bdd
     * @return bdd la bdd
     */
    public function getBdd(){
        return $this->bdd;
    }

    /**
     * Setter de l'attribut materiel
     * @param materiel un materiel
     */
    public function setMateriel($materielParam){
        $this->materiel = $materielParam;
    }

    /**
     * Setter de l'attribut bdd
     * @param bdd une bdd
     */
    public function setBdd($bddParam){
        $this->bdd = $bddParam;
    }

    /**
     * Methode permettant de creer un materiel dans la base de donnees
     * @throws Exception
     */
    public function creerMateriel(){
        
        try {
            // Insertion d'un materiel dans la table Materiel
            $sql = "INSERT INTO Materiel (ref_materiel, version_materiel, 
                                            nom_materiel, photo_materiel, type_materiel, 
                                            num_tel_materiel, statut_materiel) 
                    VALUES (:ref, :version, :nom, :photo, :type, :numtel, :statut)";
            $prep = $this->bdd->preparer_requete($sql); 
            if ($prep === FALSE){
                echo "false";
            }  
            $prep->execute(array(
                'ref' => $this->materiel->getRef(),
               	'version' => $this->materiel->getVersion(), 
                'nom' => $this->materiel->getNom(),
                'photo' => $this->materiel->getPhoto(),
                'type' => $this->materiel->getType(),
                'numtel' => $this->materiel->getNumtel(),
                'statut' => $this->materiel->getStatut(),
			));
            
        } catch(Exception $e) {
            // En cas d'erreur, on affiche un message et on arrete tout
            die('connexion échouée : '.$e->getMessage())."<br/>";

        }

    }

    /**
     * Methode permettant de modifier un materiel dans la base de données appliquee a un objet
     * dont l'attribut materiel contient les modifs ou non a effectuer
     * @throws Exception
     */
    public function modifierMateriel(){

        try {
            // Modification d'un materiel dans la table Materiel
            $sql = "UPDATE Materiel
            SET version_materiel = :version,
                nom_materiel = :nom,
                photo_materiel = :photo,
                num_tel_materiel = :numtel,
                statut_materiel = :statut
            WHERE ref_materiel = :ref";

            $prep = $this->bdd->preparer_requete($sql); 
            if ($prep === FALSE){
                echo "false";
            }  
            $prep->execute(array(
               	'version' => $this->materiel->getVersion(), 
                'nom' => $this->materiel->getNom(),
                'photo' => $this->materiel->getPhoto(),
                'numtel' => $this->materiel->getNumtel(),
                'statut' => $this->materiel->getStatut(),
                'ref' => $this->materiel->getRef(),
			));

        } catch(Exception $e) {
            // En cas d'erreur, on affiche un message et on arrete tout
            die('connexion échouée : '.$e->getMessage())."<br/>";

        }
    }

    /**
     * Methode permettant de supprimer un materiel connaissant sa reference
     * @param reference une reference de materiel
     * @throws Exception
     */
    public function supprimerMateriel($reference){

        try {
            // Suppression d'un materiel dans la table Materiel
            $sql1 = "SET FOREIGN_KEY_CHECKS=0;";
            $sql2 = "DELETE FROM Materiel WHERE ref_materiel = :ref";
            $sql3 = "SET FOREIGN_KEY_CHECKS=1;";
            $prep1 = $this->bdd->preparer_requete($sql1); 
            $prep1->execute(); 
            $prep2 = $this->bdd->preparer_requete($sql2); 
            $prep2->execute(array(
                'ref' => $reference,
            ));
            $prep3 = $this->bdd->preparer_requete($sql3); 
            $prep3->execute(); 

        } catch(Exception $e) {
            // En cas d'erreur, on affiche un message et on arrete tout
            die('connexion échouée : '.$e->getMessage())."<br/>";

        }
    }

    /**
     * Methode permettant de rendre disponible un materiel connaissant sa reference
     * @param reference une reference de materiel
     * @throws Exception
     */
    public function rendreDisponibleMateriel($reference){
        try {
            $sql = "UPDATE Materiel
            SET statut_materiel = :statut
            WHERE ref_materiel = :ref";

            $prep = $this->bdd->preparer_requete($sql);  
            $prep->execute(array(
                'statut' => 1,
                'ref' => $reference,
            ));

        } catch(Exception $e) {
            // En cas d'erreur, on affiche un message et on arrete tout
            die('connexion échouée : '.$e->getMessage())."<br/>";

        }
    }

    /**
     * Methode permettant de consulter un materiel en details connaissant sa reference
     * @param reference une reference de materiel
     * @return detail un tableau des informations du materiel consulte
     * @throws Exception
     */
    public function consulterDetailMateriel($reference){
        
        try {
            // Consultation des informations d'un utilisateur de la table Utilisateur
            $sql="SELECT m.ref_materiel, version_materiel, nom_materiel, photo_materiel, type_materiel, num_tel_materiel, 
                    statut_materiel, matricule_utilisateur, debut_emprunt, fin_emprunt FROM Materiel m LEFT JOIN Emprunter e 
                    ON m.ref_materiel = e.ref_materiel WHERE m.ref_materiel ="."'".$reference."'";
            $result = $this->bdd->lire($sql);

            $row = $result->fetch();
           
            $detail = array(
                'nom' => $row['nom_materiel'], 
                'version' => $row['version_materiel'],
                'reference' => $row['ref_materiel'],
                'photo' => $row['photo_materiel'],
                'type' => $row['type_materiel'],
                'numtel' => $row['num_tel_materiel'],  
                'statut' => $row['statut_materiel'],
                'emprunteur' => $row['matricule_utilisateur'],
                'dateDebut' => $row['debut_emprunt'],
                'dateFin' => $row['fin_emprunt'],
            );
            
        } catch(Exception $e) {
            // En cas d'erreur, on affiche un message et on arrete tout
            die('connexion échouée : '.$e->getMessage())."<br/>";

        }

        return $detail;
    }


    /**
     * Methode permettant de consulter la liste de tous les materiels
     * @return tousLesMat un tableau regroupant tous les materiels presents dans la BDD
     * @throws Exception
     */
    public function consulterMateriels(){
        
        try {
            // Consultation des informations d'un utilisateur de la table Utilisateur
            $sql = "SELECT m.ref_materiel, version_materiel, nom_materiel, photo_materiel, type_materiel, num_tel_materiel, 
                    statut_materiel, fin_emprunt FROM Materiel m LEFT JOIN Emprunter e ON m.ref_materiel = e.ref_materiel
                    ORDER BY nom_materiel";
            $result = $this->bdd->lire($sql);

            while($row = $result->fetch()) {
                $detail = array(
                    'nom' => $row['nom_materiel'], 
                    'version' => $row['version_materiel'],
                    'reference' => $row['ref_materiel'],
                    'photo' => $row['photo_materiel'],
                    'type' => $row['type_materiel'],  
                    'statut' => $row['statut_materiel'],
                    'dateFin' => $row['fin_emprunt'],
                );
                
                $tousLesMat [] = $detail;
            }
            
        } catch(Exception $e) {
            // En cas d'erreur, on affiche un message et on arrete tout
            die('connexion échouée : '.$e->getMessage())."<br/>";

        }

        return $tousLesMat;

    }

}
?>