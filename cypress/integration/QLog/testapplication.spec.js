describe('Test de l\'application', function()  {

    it('connexion site web', function()  {

        cy.visit('localhost/quallog/vue')

        cy.get(':nth-child(1) > .form-label-group > label').type('admin@univ-tours.fr')
        cy.get(':nth-child(2) > .form-label-group > label').type('admin')
        cy.get('.btn').click()
        cy.get('[href="ConsultationUtilisateurs"] > .btn').contains('Gérer les utilisateurs')
        cy.get('[href="CreationMateriel.php"] > .btn').contains('Ajouter un matériel')

    })

    it('connexion site web non fonctionnel', function()  {

        cy.visit('localhost/quallog/vue')

        cy.get(':nth-child(1) > .form-label-group > label').type('admin@univ-tours.fr')
        cy.get(':nth-child(2) > .form-label-group > label').type('admin1234')
        cy.get('.btn').click()
        //il faudrait verifier l'affichage de l'alerte de la mauvaise connexion mais nous ne savons pas comment le faire

    })

    it('creer materiel', function()  {

        cy.visit('localhost/quallog/vue')

        cy.get(':nth-child(1) > .form-label-group > label').type('admin@univ-tours.fr')
        cy.get(':nth-child(2) > .form-label-group > label').type('admin')
        cy.get('.btn').click()
        cy.get('[href="CreationMateriel.php"] > .btn').click()
        cy.get('fieldset > :nth-child(1)').type('ordi portable asus')
        cy.get('fieldset > :nth-child(2)').type('XB13.Z')
        cy.get('fieldset > :nth-child(3)').type('XX123')
        cy.get('#type').select('Ordinateur')
        cy.get('form > .btn').click()

    })

    it('consultation materiel et suppression', function()  {

        cy.visit('localhost/quallog/vue')

        cy.get(':nth-child(1) > .form-label-group > label').type('admin@univ-tours.fr')
        cy.get(':nth-child(2) > .form-label-group > label').type('admin')
        cy.get('.btn').click()
        cy.get('[onclick="window.location=\'ConsultationDetailMaterielAdmin.php?reference=AP001\'"] > :nth-child(1)').click()
        cy.get('[onclick="if(confirm(\'Voulez-vous supprimer ce materiel?\')){window.location=\'../Controleur/PHPSupprimerMateriel.php?reference=AP001\';}"]').click()

    })

    it('creation utilisateur', function()  {

        cy.visit('localhost/quallog/vue')

        cy.get(':nth-child(1) > .form-label-group > label').type('admin@univ-tours.fr')
        cy.get(':nth-child(2) > .form-label-group > label').type('admin')
        cy.get('.btn').click()
        cy.get('[href="ConsultationUtilisateurs"] > .btn').click()
        cy.get('[href="CreationUtilisateur.php"] > .btn').click()
        cy.get('fieldset > :nth-child(1)').type('Dupond')
        cy.get('fieldset > :nth-child(2)').type('Martin')
        cy.get('fieldset > :nth-child(3)').type('DE12343')
        cy.get('fieldset > :nth-child(4)').type('dupond.martin@univ-tours.fr')
        cy.get('#role').select('Emprunteur')
        cy.get('form > .btn').click()

    })

   it('bouton retour lors de la consultation en detail d\'un utilisateur', function()  {

        cy.visit('localhost/quallog/vue')

        cy.get(':nth-child(1) > .form-label-group > label').type('admin@univ-tours.fr')
        cy.get(':nth-child(2) > .form-label-group > label').type('admin')
        cy.get('.btn').click()
        cy.get('[href="ConsultationUtilisateurs"] > .btn').click()
        cy.get('[onclick="window.location=\'ConsultationDetailUtilisateur?matricule=1111111\'"] > :nth-child(1)').click()
        cy.get('a > .btn')

    })

    it('deconnexion', function()  {

        cy.visit('localhost/quallog/vue')

        cy.get(':nth-child(1) > .form-label-group > label').type('admin@univ-tours.fr')
        cy.get(':nth-child(2) > .form-label-group > label').type('admin')
        cy.get('.btn').click()
        cy.get('.glyphicon').click()
        cy.get(':nth-child(1) > .form-label-group > label').type('admin@univ-tours.fr')


    })

})