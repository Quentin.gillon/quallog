<?php
  require_once "Header.php";
  require "../Controleur/PHPDetailUtilisateur.php";
?>

<!DOCTYPE html>
 <html>
    <head>
        <meta charset='utf-8'>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
     </head>
     <body>
         <div class="container">
             <h3>Modifier le compte de l'utilisateur </h3>
             <form action="../Controleur/PHPModifUtilisateur.php?matricule=<?php echo $_GET['matricule']?>" method="post">
               <fieldset>
                 <!-- Champ nom-->
                 <div class="form-group">
                   <label for="nom">Nom</label>
                   <input type="text" class="form-control" id="nom" name="nom" value="<?php echo $detail['nom']; ?>">
                 </div>
                 <!-- Champ prenom-->
                 <div class="form-group">
                   <label for="prenom">Prénom</label>
                   <input type="text" class="form-control" id="prenom" name="prenom" value="<?php echo $detail['prenom']; ?>">
                 </div>
                 <!-- Adresse mail-->
                 <div class="form-group">
                   <label for="mail">Adresse mail</label>
                   <input type="email" class="form-control" id="mail" name="mail" value="<?php echo $detail['email']; ?>">
                 </div>
                 <!-- Liste déroulante Role-->
                 <div class="form-group">
                   <label for="role">Rôle</label>
                   <select id="role" name="role" class="form-control">
                   <?php 
                     if( $detail['role'] == 'Administrateur'){
                       $selected = 'Administrateur';
                       $noSelected = 'Emprunteur';
                     }
                     else{
                        $selected = 'Emprunteur';
                        $noSelected = 'Administrateur';
                     }
                   ?>
                      <option value="Emprunteur" selected><?php echo $selected ?></option> 
                      <option value="Administrateur"><?php echo $noSelected ?></option>
                   </select>
                 </div>

                </fieldset>
                <!-- Bouton Modifier-->
                <input type="submit" class="btn btn-primary " onclick="return confirm('Voulez-vous sauvegarder les modifications ?')" value = "Modifier">

                <!-- Bouton Retour-->
                <a href="ConsultationDetailUtilisateur">
                  <button type="button " class="btn btn-default " >Retour</button>
                </a>
            </form>
         </div>
    </body>
</html>