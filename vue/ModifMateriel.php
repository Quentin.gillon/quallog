<?php
  require_once "Header.php";
  require "../Controleur/PHPDetailMateriel.php";
?>

<!DOCTYPE html>
 <html>
    <head>
        <meta charset='utf-8'>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
     </head>
     <body>
         <div class="container">
             <h3>Modification d'un matériel <!--référence du matériel--></h3>
             <form action="../Controleur/PHPModifMateriel.php?reference=<?php echo $_GET['reference']?>" method="post">
               <fieldset>
                  <!-- Champ nom-->
                 <div class="form-group">
                   <label for="nom">Nom</label>
                   <input type="text" class="form-control" id="nom" name="nom" value="<?php echo $detail['nom']; ?>"> </input>
                 </div>
                  <!-- Champ version-->
                 <div class="form-group">
                   <label for="version">Version</label>
                   <input type="text" class="form-control" id="version" name="version" value="<?php echo $detail['version']; ?>"></input>
                 </div>
                  <!-- Champ photo-->
                 <div class="form-group">
                   <label for="photo">Photo</label>
                   <input type="url" class="form-control" id="photo" name="photo" value="<?php echo $detail['photo']; ?>"></input>
                 </div>
                  <!-- Champ telephone-->
                 <div class="form-group">
                   <label for="num">Numéro de téléphone</label>
                   <input type="tel" class="form-control" id="tel" name="tel" value="<?php echo $detail['numtel']; ?>"></input>
                 </div>
                  <!-- Liste déroulante disponibilite-->
                 <div class="form-group">
                   <label for="dispo">Disponibilité</label>
                   <?php 
                     if( $detail['statut'] == 1){
                       $selected = 'Oui';
                       $noSelected = 'Non';
                     }
                     else{
                        $selected = 'Non';
                        $noSelected = 'Oui';
                     }
                   ?>
                   <select id="disponibilite" name="disponibilite" class="form-control">
                        <option value="Oui" selected><?php echo $selected ?> </option> 
                        <option value="Non"><?php echo $noSelected ?> </option>
                   </select>
                 </div>

              </fieldset>
              <!-- Bouton Modifier-->
              <input type="submit" class="btn btn-primary " value = "Modifier" onclick="return confirm('Voulez-vous sauvegarder les modifications ?')">
            </form>
              <!-- Bouton Retour-->
              <a href="ConsultationDetailMaterielAdmin?reference=<?php echo $_GET['reference']?>">
                 <button type="button " class="btn btn-default " >Retour</button>
              </a>
            
          </div>
        </body>
</html>
