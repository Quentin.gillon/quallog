<?php
  require_once "Header.php";
  require "../Controleur/PHPDetailMateriel.php";
?>

<!DOCTYPE html>
 <html>
    <head>
        <meta charset='utf-8'>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
     </head>
     <body>
         <div class="container">
             <h3>Consultation d'un matériel <!--référence du matériel--></h3>
             <form>
               <fieldset>
                 
               <div class="form-group">
                   <label for="nom">Nom</label>
                   <output type="text" class="form-control" id="nom" onforminput="value ="><?php echo $detail['nom']; ?></output>
                 </div>
                 
                 <div class="form-group">
                   <label for="version">Version</label>
                   <output type="text" class="form-control" id="version" onforminput="value ="><?php echo $detail['version']; ?></output>
                 </div>

                 <div class="form-group">
                   <label for="reference">Référence</label>
                   <output type="text" class="form-control" id="reference" onforminput="value ="><?php echo $detail['reference']; ?></output>
                 </div>
                 
                 <div class="form-group">
                   <label for="photo">Photo</label>
                   <output type="text" class="form-control" id="photo" onforminput="value ="><?php echo $detail['photo']; ?></output>
                 </div>
                 
                 <div class="form-group">
                   <label for="numero de telephone">Numéro de téléphone</label>
                   <output type="text" class="form-control" id="numero de telephone" onforminput="value ="><?php echo $detail['numtel']; ?></output>
                 </div>
                 

                 <?php 
                  $dispo = 'Oui';
                  if($detail['statut'] == 0){
                    $dispo = 'Non';
                  }                 
                 ?>
                 <div class="form-group">
                   <label for="disponibilite">Disponibilité</label>
                   <output type="text" class="form-control" id="disponibilite" onforminput="value ="><?php echo $dispo; ?></output>
                 </div>

                 <div class="form-group">
                   <label for="emprunteur">Emprunteur</label>
                   <output type="text" class="form-control" id="emprunteur" onforminput="value ="><?php echo $detail['emprunteur']; ?></output>
                 </div>

                 <div class="form-group">
                   <label for="Date de debut d'emprunt">Date de début d'emprunt</label>
                   <output type="text" class="form-control" id="Date de debut d'emprunt" onforminput="value ="><?php echo $detail['dateDebut']; ?></output>
                 </div>

                 <div class="form-group">
                   <label for="Date de fin d'emprunt">Date de fin d'emprunt</label>
                   <output type="text" class="form-control" id="Date de fin d'emprunt" onforminput="value ="><?php echo $detail['dateFin']; ?></output>
                 </div>

               </fieldset>
             </form>
         </div>
        

         <div class="container"> 
              <!-- Bouton Emprunter-->
              <button type="button"  class="btn btn-primary " onclick="window.location='Emprunter.php?reference=<?php echo $_GET['reference'];?>'"> Emprunter </button>
              <!-- Bouton Reserver-->              
              <button type="button " class="btn btn-default " onclick="window.location='Reservation.php?reference=<?php echo $_GET['reference'];?>'"> Reserver </button>
              <!-- Bouton Retour-->
              <a href="ConsultationMaterielsEmprunteur">
                  <button type="button " class="btn btn-default " >Retour</button>
              </a>
            </div>

    </body>
</html>