<?php
  require_once "Header.php";
?>

<!DOCTYPE html>
 <html>
    <head>
        <meta charset='utf-8'>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </head>
      <body>

        <div class="container">
            <h3>Consultation de l'historique des emprunts</h3>

            <!-- Barre de recherche -->
            <div class="input-group md-form form-sm form-1 pl-0">
              <div class="input-group-prepend">
                <span class="input-group-text cyan lighten-2" id="basic-text1"><i class="fas fa-search text-white"
                    aria-hidden="true"></i></span>
              </div>
              <input class="form-control my-0 py-1" type="text" placeholder="Recherche" aria-label="Search">
            </div>

            <div class="form-group">
              <!-- Bouton Matériel-->
              <a href="ConsultationMateriels.php">
                <button type="button" class="btn btn-default"> Gérer les matériels informatiques</button>
              </a>

              <!-- Bouton Utilisateur-->
              <a href="ConsultationUtilisateurs.php">
                <button type="button " class="btn btn-default">Gérer les utilisateurs</button>
              </a>
            </div> 

            <!-- Bouton de Filtre Type-->
            <div class="form-group row">
              <div class="form-group col-md-4">
                <select id="type" class="form-control">
                  <option value="" selected>Tous les types</option> <!--option par défault-->
                  <option value="">Ordinateur</option> 
                  <option value="">Tablette</option>
                  <option value="">Téléphone</option>
                </select>
              </div>
              <div class="form-group">
                <button type="button" class="btn btn-info btn-sm">Filtrer</button>
              </div>
            </div>

            <table class="table" width="100%">
                <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Prénom</th>
                        <th>Matricule</th>
                        <th>Matériel emprunté</th> <!--nom du materiel-->
                        <th>Type</th>
                        <th>Référence</th>
                        <th>Date de début d'emprunt</th>
                        <th>Date de fin d'emprunt</th>
                    </tr>
                </thead>
                <tbody>
                     <tr>
                        <td>Nom</td>
                        <td>Prenom</td>
                        <td>0000000</td>
                        <td>MacBook Air</td>
                        <td>Ordinateur</td>
                        <td>AP333</td>
                        <td>21/12/2020</td>
                        <td>21/01/2021</td>
                    </tr>
                </tbody>
            </table>
          </div>

      </body>
</html>