<?php
  require_once "Header.php";
?>

<!DOCTYPE html>
 <html>
    <head>
        <meta charset='utf-8'>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
     </head>
     <body>
         <div class="container">
             <h3>Création d'un matériel</h3>
             <!-- Création du formulaire-->
             <form action="../Controleur/PHPCreationMateriel.php" method="post">
               <fieldset>
                  <!-- Champ nom-->                 
                  <div class="form-group">
                    <label for="nom">Nom *</label>
                    <input type="text" class="form-control" name = "nom" id="nom"></input>
                  </div>
                  <!-- Champ version-->
                  <div class="form-group">
                    <label for="version">Version * </label>
                    <input type="text" class="form-control" name = "version" id="version"></input>
                  </div>
                  <!-- Champ reference-->
                  <div class="form-group">
                    <label for="reference">Référence * </label>
                    <input type="text" class="form-control" name = "reference" id="reference"></input>
                  </div>
                  <!-- Liste déroulante type-->
                  <div class="form-group">
                    <label for="type">Type * </label>
                    <select id="type" name="type" class="form-control">
                      <option value="Ordinateur" selected>Ordinateur</option> <!--option par défault-->
                      <option value="Tablette">Tablette</option>
                      <option value="Téléphone">Téléphone</option>
                    </select>
                  </div>
                  <!-- Liste déroulante disponibilite-->
                  <div class="form-group">
                   <label for="disponibilite">Disponibilité * </label>
                    <select id="disponibilite" name="disponibilite" class="form-control">
                      <option value="Oui" selected>Oui</option> <!--option par défault-->
                      <option value="Non">Non</option>
                    </select>
                  </div>
                  <!-- Champ photo-->
                  <div class="form-group">
                    <label for="photo">Photo</label>
                    <input type="text" class="form-control" id="photo" name="photo" placeholder="url"></input>
                  </div>
                  <!-- Champ numero de telephone-->
                  <div class="form-group">
                    <label for="numero de telephone">Numéro de téléphone</label>
                    <input type="text" class="form-control" id="numero" name="numero"></input>
                  </div>

                </fieldset>
                <h6> Champs obligatoires * </h6>
                
                <!-- Bouton Valider-->
                <input type="submit" class="btn btn-primary" value = "Valider">
              </form>
              <!-- Bouton Retour-->
              <a href="ConsultationMateriels">
                <button type="button " class="btn btn-default " >Retour</button>
              </a>

              
              
          </div>
    </body>
</html>