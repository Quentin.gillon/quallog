<?php
  require_once('../Controleur/CBdd.class.php');
  require_once('../Modele/CUtilisateur.class.php');
  require_once('../Controleur/CFonctionnalitesUtilisateur.class.php');
  require_once('../Controleur/CFonctionnalitesMateriel.class.php');

  $bdd = new CBdd();
  $bdd->creer_bdd();
  $utilisateur = new CUtilisateur(1111111,"admin","istrateur","Administrateur","admin@univ-tours.fr");
  $fonctUtilisateur = new CFonctionnalitesUtilisateur($utilisateur);
  $fonctUtilisateur->creerUtilisateur();
  $fonctUtilisateur->modifierMdp("admin",1111111);

  $materiel = new CMateriel("MacBook Air","MacOS 10.14","AP001","Ordinateur",1,null,null);
  $fonctMateriel = new CFonctionnalitesMateriel($materiel);
  $fonctMateriel->creerMateriel();
?>

<!DOCTYPE html>
<html lang="fr">
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" href="sb-admin.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <title>RentAMat</title>
  <meta name="robots" content="noindex, nofollow">
</head>
<body class="bg-dark">
  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Connexion</div>
      <div class="card-body">

        <form action="../Controleur/PHPConnexion.php" method="post">
        <fieldset>
            <div class="form-group">
              <div class="form-label-group">
                <input type="text" id="email" class="form-control" name="email" required="required"/>
                <label for="email">Adresse mail</label>
              </div>
            </div>

            <div class="form-group">
              <div class="form-label-group">
                <input type="password" id="mdp" class="form-control" name="mdp" required="required"/>
                <label for="mdp">Mot de passe</label>
              </div>
            </div>

            <input type="submit" name="submit" value="Se connecter" class="btn btn-success btn-block"/>
          </fieldset>
        </form>

      </div>
    </div>
  </div>
 
  </body>
</html>
