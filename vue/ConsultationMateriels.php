<?php
  require_once "Header.php";
  require "../Controleur/PHPConsulterMateriels.php";
?>

<!DOCTYPE html>
 <html>
    <head>
        <meta charset='utf-8'>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </head>
      <body>

        <div class="container">
            <h3>Consultation des matériels informatiques : <!--référence du matériel--></h3>

            <!-- Barre de recherche -->
            <div class="input-group md-form form-sm form-1 pl-0">
              <div class="input-group-prepend">
                <span class="input-group-text cyan lighten-2" id="basic-text1"><i class="fas fa-search text-white"
                    aria-hidden="true"></i></span>
              </div>
              <input class="form-control my-0 py-1" type="text" placeholder="Recherche" aria-label="Search">
            </div>

            <!-- Bouton Utilisateur-->
            <a href="ConsultationUtilisateurs">
              <button type="button " class="btn btn-default " >Gérer les utilisateurs</button>
            </a>

            <!-- Bouton Matériel-->
            <a href="CreationMateriel.php">
              <button type="button" class="btn btn-default"> Ajouter un matériel</button>
            </a>
            
            <div class="form-group row">
              <!-- Bouton de Filtre Disponible-->
              <div class="form-group col-md-4">
                <select id="type" class="form-control">
                  <option value="" selected>Tous les matériels</option> <!--option par défault-->
                  <option value="">Disponible</option>
                  <option value="">Indisponible</option> 
                </select>
              </div>
              <!-- Bouton de Filtre Type-->
              <div class="form-group col-md-4">
                <select id="type" class="form-control">
                  <option value="" selected>Tous les types</option> <!--option par défault-->
                  <option value="">Ordinateur</option> 
                  <option value="">Tablette</option>
                  <option value="">Téléphone</option>
                </select>
              </div>
              <div class="form-group">
                <button type="button" class="btn btn-info btn-sm">Filtrer</button>
              </div>
            </div>
            
            <table class="table table-hover" width="100%">
                <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Version</th>
                        <th>Référence</th>
                        <th>Type</th>
                        <th>Disponibilité</th>
                        <th>Date de fin d'emprunt</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($tousLesMat as $materielCourant){?>
                     <tr onclick="window.location='ConsultationDetailMaterielAdmin.php?reference=<?php echo  $materielCourant['reference'];?>'">
                        <td><?php echo  $materielCourant['nom'];?></td>
                        <td><?php echo  $materielCourant['version'];?></td>
                        <td><?php echo  $materielCourant['reference'];?></td>
                        <td><?php echo  $materielCourant['type'];?></td>
                        <?php 
                          $dispo = 'Oui';
                          if($materielCourant['statut'] == 0){
                            $dispo = 'Non';
                          }                 
                        ?>
                        <td><?php echo $dispo;?></td>
                        <td><?php echo  $materielCourant['dateFin'];?></td>
                    </tr>
                  <?php } ?>
                </tbody>
            </table>
          </div>

        

      </body>
</html>