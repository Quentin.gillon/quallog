<?php
  require_once "Header.php";
?>

<!DOCTYPE html>
 <html>
 <form action="../Controleur/PHPEmprunter.php?reference=<?php echo $_GET['reference']?>&matricule=<?php echo $_SESSION['matricule']; ?>" method="post">
    <div>
      <label for="party">Date de Début :</label>
      <input type="date" id="dateDebut" name="dateDebut" value = "<?php echo date('Y-m-d'); ?>"  min ="<?php echo date('Y-m-d'); ?>" max ="<?php echo date('Y-m-d'); ?>" >
    </div>

    <div>
      <label for="party">Date de Fin :</label>
      <input type="date" id="dateFin" name="dateFin" min="<?php echo date('Y-m-d'); ?>" required >
    </div>

    <?php
      if ($_SESSION['role'] == 'Emprunteur') {
        $path='../vue/ConsultationMaterielsEmprunteur.php';      
      }else{
        $path='../vue/ConsultationMateriels.php';
      }
   ?>
    <div>
      <a href="<?php echo $path ?>">
      <button type="button" class="btn btn-primary">Envoyer</button>
      </a>
    </div>

  </form>
  </html>
