<?php
  require_once "Header.php";
?>

<!DOCTYPE html>
 <html>
    <head>
        <meta charset='utf-8'>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
     </head>
     <body>
          <div class="container">
             <h1>Création d'un utilisateur</h1>
             <!-- Création du formulaire-->
             <form action="../Controleur/PHPCreationUtilisateur.php" method="post">
               <fieldset>
                  <!-- Champ nom-->
                  <div class="form-group">
                    <label for="nom">Nom *</label>
                    <input type="text" class="form-control" name = "nom" id="nom"></input>
                  </div>
                  <!-- Champ Prenom-->
                  <div class="form-group">
                    <label for="prenom">Prénom *</label>
                    <input type="text" class="form-control" name = "prenom" id="prenom"></input>
                  </div>
                  <!-- Champ Matricule-->
                  <div class="form-group">
                    <label for="matricule">Matricule *</label>
                    <input type="text" class="form-control" name = "matricule" id="matricule"></input>
                  </div>
                  <!-- Champ Adreese mail-->
                  <div class="form-group">
                    <label for="adresse mail">Adresse Mail *</label>
                    <input type="text" class="form-control" name = "adresseMail" id="adresse mail"></input>
                  </div>
                  <!-- Liste déroulante Role-->
                  <div class="form-group">
                    <label for="role">Rôle *</label>
                    <select id="role" name = "role" class="form-control">
                        <option value="Emprunteur" selected>Emprunteur</option> <!--option par défault-->
                        <option value="Administrateur">Administrateur</option>
                    </select>
                  </div>
                </fieldset>

                <h6> Champs obligatoires * </h6>

                <!-- Bouton Valider-->                
                <input type="submit" class="btn btn-primary " value = "Valider"></button>                
              </form>
              <!-- Bouton Retour-->                
              <a href="ConsultationUtilisateurs">
                <button type="button " class="btn btn-default " >Retour</button>
              </a>
            
              
                           
          </div>
      </body>
  </html>