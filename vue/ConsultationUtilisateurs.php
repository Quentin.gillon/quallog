<?php
  require_once "Header.php";
  require "../Controleur/PHPConsulterUtilisateurs.php";
?>

<!DOCTYPE html>
 <html>
    <head>
        <meta charset='utf-8'>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </head>
      <body>
        <div class="container">
            <h3>Consultation des utilisateurs <!--référence du matériel--></h3>

             <!-- Barre de recherche -->
             <div class="input-group md-form form-sm form-1 pl-0">
              <div class="input-group-prepend">
                <span class="input-group-text cyan lighten-2" id="basic-text1"><i class="fas fa-search text-white"
                    aria-hidden="true"></i></span>
              </div>
              <input class="form-control my-0 py-1" type="text" placeholder="Recherche" aria-label="Search">
            </div>

            <!-- Bouton de Filtre Rôle-->
            <div class="form-group row">
              <div class="form-group col-md-4">
                <select id="type" class="form-control">
                  <option value="" selected>Tous les rôles</option> <!--option par défault-->
                  <option value="">Administrateur</option> 
                  <option value="">Emprunteur</option>
                </select>
              </div>
              <div class="form-group">
                <button type="button" class="btn btn-info btn-sm">Filtrer</button>
              </div>
            </div>

            <!-- Bouton Matériels-->
            <a href="ConsultationMateriels.php">
              <button type="button" class="btn btn-default">Gérer les matériels informatiques</button>
            </a>

            <!-- Bouton ajout utilisateur-->
            <a href="CreationUtilisateur.php">
              <button type="button" class="btn btn-default"> Ajouter un utilisateur</button>
            </a>

            <!-- Bouton consultation historique des emprunts-->
            <a href="HistoriqueEmprunts.php">
              <button type="button" class="btn btn-default"> Historique des emprunts</button>
            </a>

            <table class="table table-hover" width="100%">
                <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Prénom</th>
                        <th>Adresse mail</th>
                        <th>Matricule</th>
                        <th>Rôle</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($tousLesUtil as $utilisateurCourant){?>
                     <tr onclick="window.location='ConsultationDetailUtilisateur?matricule=<?php echo  $utilisateurCourant['matricule'];?>'">
                        <td><?php echo  $utilisateurCourant['nom'];?></td>
                        <td><?php echo  $utilisateurCourant['prenom'];?></td>
                        <td><?php echo  $utilisateurCourant['email'];?></td>
                        <td><?php echo  $utilisateurCourant['matricule'];?></td>
                        <td><?php echo  $utilisateurCourant['role'];?></td>
                    </tr>
                  <?php } ?>
                </tbody>
            </table>
          </div>

        

      </body>
</html>