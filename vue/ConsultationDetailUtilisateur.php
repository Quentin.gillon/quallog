<?php
  require_once "Header.php";
  require "../Controleur/PHPDetailUtilisateur.php";
?>

<!DOCTYPE html>
 <html>
    <head>
        <meta charset='utf-8'>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
     </head>
     <body>
         <div class="container">
             <h3>Consultation d'un utilisateur : <?php echo $_GET['matricule']; ?></h3>
             <form>
               <fieldset>
                 
                 <div class="form-group">
                   <label for="nom">Nom</label>
                   <output type="text" class="form-control" id="nom" onforminput="value ="><?php echo $detail['nom']; ?></output>
                 </div>
                 
                 <div class="form-group">
                   <label for="prenom">Prénom</label>
                   <output type="text" class="form-control" id="prenom" onforminput="value ="><?php echo $detail['prenom']; ?></output>
                 </div>

                 <div class="form-group">
                   <label for="matricule">Matricule</label>
                   <output type="text" class="form-control" id="matricule" onforminput="value ="><?php echo $detail['matricule']; ?></output>
                 </div>
                 
                 <div class="form-group">
                   <label for="adresse mail">Adresse Mail</label>
                   <output type="text" class="form-control" id="adresse mail" onforminput="value ="><?php echo $detail['email']; ?></output>
                 </div>
                 
                 <div class="form-group">
                   <label for="role">Rôle</label>
                   <output type="text" class="form-control" id="role" onforminput="value ="><?php echo $detail['role']; ?></output>
                 </div>
                 

               </fieldset>
             </form>
         </div>

         <div class="container">
            <h3>Matériels empruntés : <!--référence du matériel--></h3>
            <table class="table" width="100%">
                <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Version</th>
                        <th>Référence</th>
                        <th>Type</th>
                        <th>Date d'emprunt</th>
                        <th>Date de retour</th>
                    </tr>
                </thead>
                <tbody>
                     <tr>
                        <td>cell1_1</td>
                        <td>cell2_1</td>
                         <td>cell3_1</td>
                         <td>cell4_1</td>
                         <td>cell5_1</td>
                         <td>cell6_1</td>
                    </tr>
                </tbody>
            </table>
        </div>

        </br>

        <div class="container"> 

          <!-- Bouton Modifier-->
              <button type="button " class="btn btn-primary " onclick="window.location='ModifUtilisateur.php?matricule=<?php echo $_GET['matricule'];?>'">Modifier</button>
          <!-- Bouton Retour-->
          <a href="ConsultationUtilisateurs">
              <button type="button " class="btn btn-default " >Retour</button>
          </a>

        </div>

    </body>
</html>